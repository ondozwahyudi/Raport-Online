<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"
      integrity="sha256-UzFD2WYH2U1dQpKDjjZK72VtPeWP50NoJjd26rnAdUI="
      crossorigin="anonymous"
    />

    <link
      href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900&display=swap"
      rel="stylesheet"
    />

    <link rel="stylesheet" href="{{asset('/frontend')}}/css/icon-font.css" />
    <link rel="stylesheet" href="{{asset('/frontend')}}/css/style.css" />
    <link rel="shortcut icon" type="image/png" href="{{asset('/frontend')}}/img/lapomark.png" />

    <title>LapoMark | Realtime Monitoring Raport</title>
  </head>
    <body>
      <div class="navigation">
          <input type="checkbox" class="navigation__checkbox" id="navi-toggle" />

          <label for="navi-toggle" class="navigation__button">
            <span class="navigation__icon">&nbsp;</span>
          </label>

          <div class="navigation__background">&nbsp;</div>

          <div class="navigation__nav">
            <ul class="navigation__list">
              <li class="navigation__item">
                <a href="{{ route('home_site')}}" class="navigation__link"
                  ><span>01</span> Home</a
                >
              </li>

                <li class="navigation__item">
                  <a href="  {{route('price')}}" class="navigation__link"
                    ><span>03</span> Harga Produk</a
                  >
                </li>
                <li class="navigation__item">
                  <a href=" {{route('story')}}" class="navigation__link"
                    ><span>04</span> Testimoni Pelanggan</a
                  >
                </li>
                <li class="navigation__item">
                  <a href="{{route('pesan_sekarang_site')}}" class="navigation__link"
                    ><span>05</span> Pesan Sekarang</a
                  >
                </li>
                <li class="navigation_item">
                  <a href="{{ route('login') }}" class="navigation__link">
                    <span>06</span> Login
                  </a>
                </li>
            </ul>
          </div>
        </div>

     @yield('contents')

     <footer class="footer">
        <div class="footer__logo-box">
          <img src=" {{asset('/frontend')}}/img/lapomark.png" alt="Logo LapoMark" class="footer__logo" />
          <h1 class="heading--footer">LapoMark</h1>
        </div>

        <div class="row">
          <div class="col-1-of-2">
            <div class="footer__navigation">
              <ul class="footer__list">
                <li class="footer__item">
                  <a href="#" class="footer__link">Company</a>
                </li>
                <li class="footer__item">
                  <a href="#l" class="footer__link">Contact Us</a>
                </li>

                <li class="footer__item">
                  <a href=" {{route('privacy') }}" class="footer__link">Privacy Policy</a>
                </li>
                <li class="footer__item">
                  <a href=" {{route('faq') }} " class="footer__link">FAQ</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-1-of-2">
            <div class="footer__social">
              <p>
                LapoMark adalah sebuah perusahaan yang bergelut pada bidang
                pendidikan yang dikombinasikan dengan teknologi agar kedepan
                fasilitas pendidikan kita tak kalah jauh dengan diluar negeri
                sana.
              </p>
              <h3 class="footer__social--title">Our Social Media</h3>
              <ul class="footer__list">
                  <li class="footer__item">
                    <a href="#" class="footer__link">
                      <i class="footer__link--icon fab fa-facebook"></i>
                    </a>
                  </li>

                  <li class="footer__item">
                    <a href="#" class="footer__link">
                      <i class="footer__link--icon fab fa-twitter"></i>
                    </a>
                  </li>

                  <li class="footer__item">
                    <a href="#" class="footer__link">
                      <i class="footer__link--icon fab fa-instagram"></i>
                    </a>
                  </li>
                  <li class="footer__item">

                      <a href="https://api.whatsapp.com/send?phone=6281918135527&text=Halo,%20Lapomark" class="footer__link">
                        <i class="footer__link--icon fab fa-whatsapp"></i>
                      </a>
                    </li>
                </ul>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="footer__copyright">
            &copy; 2019 | Made with <i class="icon-basic-heart"></i> by LapoMark
          </div>
        </div>
    </body>
  </html>
