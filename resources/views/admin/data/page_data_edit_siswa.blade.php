@extends('layouts.main_navigation')
@section('header')
{{-- // stylis links css --}}
<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>

@endsection
@section('contents_page')
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Table</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="#">Data Siswa</a></li>
                <li class="breadcrumb-item active">Edit Data Siswa</li>
                </ol>
            </div>
            </div>
        </div>
    </section>
          @if (session("success"))
          <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;
                  </span>
              </button>
    
              {{ session("success")}}
              <strong> Well done!
              </strong>
          </div>
          @endif
      <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header">
                        <div class="modal-body">
                            @if (count($errors) > 0)
                            <div>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    {{ $error }}
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form action="{{route('update_action', $siswa->nis) }}" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('nis') ? 'has-error' : '' }}">
                                        <label for="nis">Nis</label>
                                        <input type="text" class="form-control" id="nis" name="nis" placeholder="Nis" value="{{ $siswa->nis }}" autocomplete="off" required disabled>
                                        @if ($errors->has('nis'))
                                            <span class="help-block">{{$errors->first('nis')}}</span>
                                        @endif  
                                    </div>
                                    <div class="form-group {{ $errors->has('nama_siswa') ? 'has-error' : ''}}">
                                        <label for="nama_siswa">Nama Siswa</label>
                                        <input type="text" class="form-control" id="nama_siswa" name="nama_siswa" placeholder="Nama Siswa" value="{{ $siswa->nama_siswa }}" autocomplete="off" required>
                                        @if ($errors->has('nama_siswa'))
                                            <span class="help-block">{{$errors->first('nama_siswa')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Kelas</label>
                                        <select name="kelas_id" class="form-control" >
                                            <option value="{{$siswa->kelas_id}}">{{$siswa->kelas->kelas_nama}}</option>
                                            @foreach ($kelas as $data_kelas)
                                            <option value="{{$data_kelas->id}}" @if($siswa->kelas_id == '{{$data_kelas->id}}') selected @endif>{{$data_kelas->kelas_nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="form-group {{ $errors->has('jenis_kelamin') ? 'has-error' : ''}}" >
                                        <label for="jenis_kelamin">Jenis Kelamin</label>
                                        <select name="jenis_kelamin" id="jenis_kelamin" name="jenis_kelamin" class="form-control" required>
                                            <option value="">Jenis Kelamin</option>
                                            <option value="L" @if($siswa->jenis_kelamin == 'L') selected @endif >Laki-Laki</option>
                                            <option value="P" @if($siswa->jenis_kelamin == 'P' )  selected @endif  >Prempuan</option>
                                        </select>
                                        @if ($errors->has('jenis_kelamin'))
                                            <span class="help-block">{{$errors->first('jenis_kelamin')}}</span>
                                        @endif
                                    </div>

                                    <div class="form-group {{$errors->has('tanggal_lahir') }}">
                                        <label for="tanggal_lahir">Tanggal Lahir</label>
                                        <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir" value="{{$siswa->tanggal_lahir }}" required> 
                                    @if ($errors->has('tanggal_lahir'))
                                        <span class="help-block">{{$errors->first('tanggal_lahir')}}</span>
                                    @endif                
                                    </div>

                                    <div class="form-group {{ $errors->has('agama') ? 'has-error' : ''}}">
                                        <label for="agama">Agama</label>
                                        <select  id="agama" name="agama" class="form-control">
                                            <option value="">Pilih Agama Anda</option>
                                            <option value="Islam"  @if ($siswa->agama == 'Islam') selected @endif>Islam</option>
                                            <option value="Kristen" @if ($siswa->agama == 'Kristen') selected @endif>Kristen</option>
                                            <option value="Hindu" @if ($siswa->agama == 'Hindu') selected @endif>Hindu</option>
                                            <option value="Budha" @if ($siswa->agama == 'Budha') selected @endif>Budha</option>
                                            <option value="Lainnya" @if ($siswa->agama == 'Lainnya') selected @endif>Lainnya</option>
                                        </select>
                                    @if ($errors->has('agama'))
                                        <span class="help-block">{{$errors->first('agama')}}</span>
                                    @endif  
                                    </div>

                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ $siswa->user()->email }}" autocomplete="off" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">{{$errors->first('email')}}</span>
                                    @endif 
                                    </div>
                                    <div class="form-group {{ $errors->has('nama_ortu') ? 'has-error' : ''}}">
                                        <label for="nama_ortu">Nama Orang Tua</label>
                                        <input type="text" class="form-control" id="nama_ortu" name="nama_ortu" placeholder="Nama Orang Tua" value="{{ $siswa->nama_ortu }}" autocomplete="off" required>
                                    @if ($errors->has('nama_ortu'))
                                        <span class="help-block">{{$errors->first('nama_ortu')}}</span>
                                    @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('pekerjaan') ? 'has-error' : ''}}">
                                        <label for="pekerjaan_orang_tua">Pekerjaan Orang Tua</label>
                                        <input type="text" class="form-control" id="pekerjaan_orang_tua" name="pekerjaan" placeholder="Pekerjaan Orang Tua" value="{{ $siswa->pekerjaan }}" autocomplete="off" required>
                                    @if ($errors->has('pekerjaan'))
                                        <span class="help-block">{{$errors->first('pekerjaan')}}</span>
                                    @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('alamat') ? 'has-error' : ''}}">
                                        <label for="alamat">Alamat</label>
                                        <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" value="{{ $siswa->alamat }}" autocomplete="off" required>
                                    @if ($errors->has('alamat'))
                                        <span class="help-block">{{$errors->first('alamat')}}</span>
                                    @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('no_telp') ? 'has-error' : ''}}">
                                        <label for="nomer_telp">Nomer Telp</label>
                                        <input type="text" class="form-control" id="nomer_telp" name="no_telp" placeholder="Nomer Telp" value="{{ $siswa->no_telp }}" autocomplete="off" required>
                                    @if ($errors->has('no_telp'))
                                        <span class="help-block">{{$errors->first('no_telp')}}</span>
                                    @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('foto') ? 'has-error' : ''}}">
                                        <div class="input-group" >
                                            <label class="input-group-btn" >
                                                <span class="btn btn-primary">
                                                    Browse&hellip; <input type="file" style="display: none;"  class="form-control" name="upload_foto" multiple>
                                                </span>
                                            </label>
                                            <input type="text" class="form-control" readonly>
                                        </div>
                                        @if ($errors->has('no_telp'))
                                            <span class="help-block">{{$errors->first('foto')}}</span>
                                        @endif
                                    </div>

                                <!-- /.box-body -->
                                <div class="modal-footer">
                                    <a href="{{route('admin_page_siswa')}}" class="btn btn-danger" data-dismiss="modal">Back</a>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                              
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
{{-- scrpit js --}}
<script>


</script>
@endsection