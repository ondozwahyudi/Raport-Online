@extends('layouts.frontend')
@section('contents')


    <header class="header header--book">
      <div class="header__logo-box">
        <img src=" {{asset('/frontend')}}/img/lapomark.png" alt="Logo" class="header__logo" />
      </div>

      <div class="header__text-box">
        <h1 class="heading-primary--large">
          <span class="heading-primary">
            Harga yang kami tawarkan
          </span>
        </h1>
        <p class="paragraph paragraph--white">
          Berikut paket-paket harga yang kami tawarkan, setiap harga memiliki
          spesifikasi berbeda.
        </p>

        <a href="#prices" class="btn btn--white btn--animated">Lihat Harga</a>
      </div>
    </header>

    <main>
      <section class="section-price" id="prices">
        <div class="u-center-text ">
          <h2 class="heading-secondary u-margin-bottom-medium">
            Harga Yang Kami Tawarkan
          </h2>
        </div>

        <div class="row">
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--1">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--1">
                    Paket Coba
                  </span>
                </h4>

                <div class="card__details">
                  <ul>
                    <li>Masa Aktif 1 Bulan</li>
                    <li>Akses Admin</li>
                    <li>Slow Respon</li>
                  </ul>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-1">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$297</p>
                  </div>

                  <a href="book.html" class="btn btn--white">Pesan Sekarang</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--2">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--2">
                    Paket Nyoba
                  </span>
                </h4>

                <div class="card__details">
                  <ul>
                    <li>Masa Aktif 6 Bulan</li>
                    <li>Gratis Domain</li>
                    <li>Full Admin Page</li>
                    <li>Support Medium Respon</li>
                  </ul>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-2">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$697</p>
                  </div>

                  <a href="book.html" class="btn btn--white">Pesan Sekarang</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--3">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--3">
                    Paket Nyaman
                  </span>
                </h4>

                <div class="card__details">
                  <ul>
                    <li>Masa Aktif 1 Tahun</li>
                    <li>Gratis Domain</li>
                    <li>Gratis Server</li>
                    <li>Full Support</li>
                    <li>Konsultasi 24 Jam</li>
                  </ul>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-3">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$897</p>
                  </div>

                  <a href="book.html" class="btn btn--white">Pesan Sekarang</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--3">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--3">
                    Paket Nembak
                  </span>
                </h4>

                <div class="card__details">
                  <ul>
                    <li>Masa Aktif 2 Tahun</li>
                    <li>Gratis Domain</li>
                    <li>Gratis Server</li>
                    <li>Full Support</li>
                    <li>Konsultasi 24 Jam</li>
                  </ul>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-3">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$1097</p>
                  </div>

                  <a href="book.html" class="btn btn--white">Pesan Sekarang</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--2">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--2">
                    Paket Sayang
                  </span>
                </h4>

                <div class="card__details">
                  <ul>
                    <li>Masa Aktif 3 Tahun</li>
                    <li>Gratis Domain</li>
                    <li>Gratis Server</li>
                    <li>Full Support</li>
                    <li>Konsultasi 24 Jam</li>
                  </ul>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-2">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$1597</p>
                  </div>

                  <a href="book.html" class="btn btn--white">Pesan Sekarang</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--1">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--1">
                    Paket Cinta
                  </span>
                </h4>

                <div class="card__details">
                  <ul>
                    <li>Masa Aktif 5 Tahun</li>
                    <li>Gratis Domain</li>
                    <li>Gratis Server</li>
                    <li>Full Support</li>
                    <li>Konsultasi 24 Jam</li>
                  </ul>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-1">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$2997</p>
                  </div>

                  <a href="book.html" class="btn btn--white">Pesan Sekarang</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
@endsection