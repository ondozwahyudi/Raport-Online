<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\User;
use App\Mapel;
use App\Guru;
use App\Kelas;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        {
            $siswa        = Siswa::all();
            $mapel        = Mapel::all();
            $guru         = Guru::all();
            $kelas        = Kelas::all();
            //sum all data table
            $jumlah       = $siswa->count();
            $jumlah_kelas = $kelas->count();
            $jumlah_mapel = $mapel->count();
            $jumlah_guru  = $guru->count();
            
            return view('admin.dashboard.page_dashboard',['jumlah' => $jumlah,'jumlah_mapel' => $jumlah_mapel,'jumlah_guru' => $jumlah_guru,'jumlah_kelas' => $jumlah_kelas]);
        }
    }
}
