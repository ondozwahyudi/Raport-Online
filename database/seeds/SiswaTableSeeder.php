<?php

use Illuminate\Database\Seeder;

class SiswaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('siswa')->delete();
        
        \DB::table('siswa')->insert(array (
            0 => 
            array (
                'id' => 17,
                'user_id' => 34,
                'kelas_id' => 1,
                'nis' => 191001,
                'nama_siswa' => 'Gilang wahyudi',
                'tanggal_lahir' => '1998-01-01',
                'jenis_kelamin' => 'L',
                'nama_ortu' => 'Wahyudi',
                'agama' => 'Islam',
                'alamat' => 'Lombok',
                'pekerjaan' => 'Petani',
                'no_telp' => '081918135527',
                'upload_foto' => '25348386_1571552769589243_8346817384468637655_n.jpg',
                'created_at' => '2019-06-25 02:48:34',
                'updated_at' => '2019-07-03 14:14:10',
            ),
            1 => 
            array (
                'id' => 18,
                'user_id' => 35,
                'kelas_id' => 1,
                'nis' => 191002,
                'nama_siswa' => 'Afochar SR',
                'tanggal_lahir' => '1999-01-15',
                'jenis_kelamin' => 'L',
                'nama_ortu' => 'Rusmanto',
                'agama' => 'Islam',
                'alamat' => 'Lombok',
                'pekerjaan' => 'Petani',
                'no_telp' => '081918135527',
                'upload_foto' => '1561535433.jpg',
                'created_at' => '2019-06-26 02:06:25',
                'updated_at' => '2019-07-03 14:14:17',
            ),
            2 => 
            array (
                'id' => 19,
                'user_id' => 36,
                'kelas_id' => 1,
                'nis' => 191003,
                'nama_siswa' => 'Arsanda Maulana',
                'tanggal_lahir' => '1999-02-02',
                'jenis_kelamin' => 'L',
                'nama_ortu' => 'Maulana',
                'agama' => 'Islam',
                'alamat' => 'Lombok',
                'pekerjaan' => 'Petani',
                'no_telp' => '081918135527',
                'upload_foto' => '1561535600.jpg',
                'created_at' => '2019-06-26 03:06:56',
                'updated_at' => '2019-07-03 14:14:26',
            ),
            3 => 
            array (
                'id' => 20,
                'user_id' => 37,
                'kelas_id' => 1,
                'nis' => 191004,
                'nama_siswa' => 'Dhimas Prayoga',
                'tanggal_lahir' => '1999-03-03',
                'jenis_kelamin' => 'L',
                'nama_ortu' => 'Prayoga',
                'agama' => 'Islam',
                'alamat' => 'Temanggung',
                'pekerjaan' => 'Petani',
                'no_telp' => '081918135527',
                'upload_foto' => '1561535684.jpg',
                'created_at' => '2019-06-26 07:54:30',
                'updated_at' => '2019-07-03 14:14:04',
            ),
            4 => 
            array (
                'id' => 21,
                'user_id' => 38,
                'kelas_id' => 1,
                'nis' => 191005,
                'nama_siswa' => 'Sidik Satiawan Yusuf',
                'tanggal_lahir' => '1998-12-21',
                'jenis_kelamin' => 'L',
                'nama_ortu' => 'Satiawan',
                'agama' => 'Islam',
                'alamat' => 'Yogyakarta',
                'pekerjaan' => 'Petani',
                'no_telp' => '08112281191',
                'upload_foto' => NULL,
                'created_at' => '2019-06-26 13:05:37',
                'updated_at' => '2019-07-03 14:13:57',
            ),
            5 => 
            array (
                'id' => 22,
                'user_id' => 40,
                'kelas_id' => 1,
                'nis' => 191006,
                'nama_siswa' => 'Iqbal Maulna Junaidi',
                'tanggal_lahir' => '2006-03-05',
                'jenis_kelamin' => 'L',
                'nama_ortu' => 'Muh Ahyar',
                'agama' => 'Islam',
                'alamat' => 'Lombok',
                'pekerjaan' => 'Petai',
                'no_telp' => '081918135527',
                'upload_foto' => '1561811787.jpg',
                'created_at' => '2019-06-29 06:55:05',
                'updated_at' => '2019-06-29 12:36:28',
            ),
            6 => 
            array (
                'id' => 23,
                'user_id' => 41,
                'kelas_id' => 1,
                'nis' => 191007,
                'nama_siswa' => 'Muhammad Fatir Al-Farizi',
                'tanggal_lahir' => '1998-09-03',
                'jenis_kelamin' => 'L',
                'nama_ortu' => 'Muh Ahyar',
                'agama' => 'Islam',
                'alamat' => 'Keruak',
                'pekerjaan' => 'Petani',
                'no_telp' => '081918135527',
                'upload_foto' => NULL,
                'created_at' => '2019-07-02 12:09:55',
                'updated_at' => '2019-07-02 12:09:55',
            ),
        ));
        
        
    }
}