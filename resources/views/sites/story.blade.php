@extends('layouts.frontend')
@section('contents')

    <header class="header">
      <div class="header__logo-box">
        <img src="{{asset('/frontend')}}/img/lapomark.png" alt="Logo" class="header__logo" />
      </div>

      <div class="header__text-box">
        <h1 class="heading-primary">
          <span class="heading-primary">
            500+ Sekolah telah di hampir 33 propinsi menggunakan LapoMark.
          </span>
        </h1>
        <p class="paragraph paragraph--white">
          Dengan banyaknya sekolah yang telah menggunakan aplikasi ini tentunya
          anda tak perlu ragi lagi untuk memercayai kami. Baca cerita mereka di
          bawah.
        </p>

        <a href="#stories" class="btn btn--white btn--animated">Lihat Fitur</a>
      </div>
    </header>

    <main>
      <section class="section-stories story--background" id="stories">
        <div class="u-center-text u-margin-bottom-large">
          <h1 class="heading-primary">
            <span class="heading-primary--large"
              >Cerita dari pelanggan kami</span
            >
          </h1>
          <p class="paragraph paragraph--white">
            Kami tidak ingin terlalu sombong, cukup mereka yang bicara
          </p>
        </div>

        <div class="row">
          <div class="story">
            <figure class="story__shape">
              <img src="{{asset('/frontend')}}/img/raisa.jpg" alt="Raisa" class="story__img" />
              <figcaption class="story__caption">Mantan Raisa</figcaption>
            </figure>

            <div class="story__text">
              <h3 class="heading-tertiary">
                Raisa Andriana ( Penyanyi )
              </h3>
              <a href="https://www.instagram.com/raisa6690/" class="btn-link">
                @raisa6690
              </a>
              <p>
                Alhamdulillah, anak saya baru lahir nih, jadi melihat teknologi
                yang baru seperti ini. Maka kita sebagai orang tua tidak
                khawatir lagi.
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="story">
            <figure class="story__shape">
              <img
                src="{{asset('/frontend')}}/img/isyana.jpg"
                alt="Isyana Sarasvati"
                class="story__img"
              />
              <figcaption class="story__caption">
                Bebeb Isyana
              </figcaption>
            </figure>

            <div class="story__text">
              <h3 class="heading-tertiary">
                Isyana Sarasvati ( Penyanyi )
              </h3>
              <a
                href="https://www.instagram.com/isyanasarasvati/"
                class="btn-link"
              >
                @isyanasarasvati
              </a>
              <p>
                Sebagai calon orang tua, tentunya kami mengharapkan yang baik
                dengan pendidikan yang oke. LapoMark hadir sebagai solusi yang
                memuaskan.
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="story">
            <figure class="story__shape">
              <img src="{{asset('/frontend')}}/img/maudy.jpg" alt="Yayang Maudy" class="story__img" />
              <figcaption class="story__caption">
                Yayang Maudy
              </figcaption>
            </figure>

            <div class="story__text">
              <h3 class="heading-tertiary">
                Yayang Maudy ( Aktris )
              </h3>
              <a href="https://www.instagram.com/maudyayunda/" class="btn-link">
                @maudyayunda
              </a>
              <p>
                Sebagai aktris dan penyanyi kami juga butuh pendidikan yang top.
                Saya melihat LapoMark sama seperti di luar negeri seperti kampus
                saya : Harvard.
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="story">
            <figure class="story__shape">
              <img
                src="{{asset('/frontend')}}/img/tretan.jpg"
                alt="Tretan Muslim"
                class="story__img"
              />
              <figcaption class="story__caption">
                Tretan Muslim
              </figcaption>
            </figure>

            <div class="story__text">
              <h3 class="heading-tertiary">
                Tretan Muslim ( Komedian Buronan )
              </h3>
              <a
                href="https://www.instagram.com/tretanmuslim/"
                class="btn-link"
              >
                @tretanmuslim
              </a>
              <p>
                Pendidikan itu penting dikarenakan kaum deadwood (kayumati /
                pengangguran) semakin berjamur. LapoMark hadir agar anak tetap
                tidak bar-bar.
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="story">
            <figure class="story__shape">
              <img
                src="{{asset('/frontend')}}/img/tejoncuk.jpg"
                alt="Sudjiwo Tejo"
                class="story__img"
              />
              <figcaption class="story__caption">
                Sudjiwo Tejo
              </figcaption>
            </figure>

            <div class="story__text">
              <h3 class="heading-tertiary">
                Sudjiwo Tejo ( President Jancukers )
              </h3>
              <a
                href="https://www.instagram.com/president_jancukers/"
                class="btn-link"
              >
                @president_jancukers
              </a>
              <p>
                Pada dasarnya pendidikan itu bukan soal unggul-unggulan.
                LapoMark hadir sebagai solusi agar perangkat pendidikan bisa
                instropeksi diri.
              </p>
            </div>
          </div>
        </div>
      </section>
    </main>
@endsection