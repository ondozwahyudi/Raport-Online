<?php

use Illuminate\Database\Seeder;

class MapelTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mapel')->delete();
        
        \DB::table('mapel')->insert(array (
            0 => 
            array (
                'id' => 7,
                'kode' => 'B-0001',
                'mapel' => 'Bahasa Indonesia',
                'guru_id' => 6,
                'created_at' => '2019-06-27 10:50:45',
                'updated_at' => '2019-07-03 11:47:59',
            ),
            1 => 
            array (
                'id' => 8,
                'kode' => 'Y-0003',
                'mapel' => 'Bahasa Inggris',
                'guru_id' => 11,
                'created_at' => '2019-06-28 12:34:31',
                'updated_at' => '2019-07-03 11:49:41',
            ),
            2 => 
            array (
                'id' => 9,
                'kode' => 'M-0001',
                'mapel' => 'Matematika',
                'guru_id' => 12,
                'created_at' => '2019-06-28 12:35:51',
                'updated_at' => '2019-07-03 11:49:11',
            ),
            3 => 
            array (
                'id' => 10,
                'kode' => 'E-0002',
                'mapel' => 'Ekonomi',
                'guru_id' => 8,
                'created_at' => '2019-06-28 12:36:13',
                'updated_at' => '2019-07-03 11:49:02',
            ),
            4 => 
            array (
                'id' => 11,
                'kode' => 'B-0002',
                'mapel' => 'Biologi',
                'guru_id' => 7,
                'created_at' => '2019-06-28 12:36:37',
                'updated_at' => '2019-07-03 11:48:57',
            ),
            5 => 
            array (
                'id' => 12,
                'kode' => 'R-0003',
                'mapel' => 'Agama',
                'guru_id' => 10,
                'created_at' => '2019-06-28 14:11:40',
                'updated_at' => '2019-07-03 11:49:17',
            ),
            6 => 
            array (
                'id' => 13,
                'kode' => 'A-0001',
                'mapel' => 'Penjaskes',
                'guru_id' => 5,
                'created_at' => '2019-06-29 11:21:34',
                'updated_at' => '2019-07-03 11:27:45',
            ),
            7 => 
            array (
                'id' => 14,
                'kode' => 'S-0003',
                'mapel' => 'Sejarah',
                'guru_id' => 13,
                'created_at' => '2019-07-03 03:43:49',
                'updated_at' => '2019-07-03 11:49:23',
            ),
            8 => 
            array (
                'id' => 15,
                'kode' => 'F-0001',
                'mapel' => 'Fisika',
                'guru_id' => 14,
                'created_at' => '2019-07-03 11:50:07',
                'updated_at' => '2019-07-03 11:50:07',
            ),
            9 => 
            array (
                'id' => 16,
                'kode' => 'S-0001',
                'mapel' => 'Sosiologi',
                'guru_id' => 9,
                'created_at' => '2019-07-03 11:50:45',
                'updated_at' => '2019-07-03 11:50:45',
            ),
        ));
        
        
    }
}