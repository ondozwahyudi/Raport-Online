@extends('layouts.frontend')
@section('contents')

  <header class="header header--book">
    <div class="header__logo-box">
      <img src=" {{asset('/frontend')}}/img/lapomark.png" alt="Logo" class="header__logo" />
    </div>

    <div class="header__text-box">
      <h1 class="heading-primary--large">
        <span class="heading-primary">
          Kontak Team Kami
        </span>
      </h1>
      <p class="paragraph paragraph--white">
        Silahkan isi form dibawah dan team support 24 jam kami akan membalas
        secepat-cepatnya melalui data yang anda kirimkan.
      </p>

      <a href="#book" class="btn btn--white btn--animated">Hubungi Kami</a>
    </div>
  </header>

  <main>
    <section class="section-book" id="book">
      <div class="row">
        <div class="book">
          <div class="book__form">
            <form action="#" class="form">
              <div class="u-margin-bottom-medium">
                <h2 class="heading-secondary">
                  Pesan sekarang
                </h2>
              </div>
              <div class="form__group">
                <input
                  type="text"
                  class="form__input"
                  placeholder="Full Name"
                  id="name"
                  required
                />
                <label for="name" class="form__label">Full Name</label>
              </div>

              <div class="form__group">
                <input
                  type="email"
                  class="form__input"
                  placeholder="Company E-mail"
                  id="email"
                  required
                />
                <label for="email" class="form__label">Company E-mail</label>
              </div>

              <div class="form__group">
                <input
                  type="text"
                  class="form__input"
                  placeholder="Company Name"
                  id="name"
                  required
                />
                <label for="name" class="form__label">Company Name</label>
              </div>

              <div class="form__group">
                <input
                  type="number"
                  class="form__input"
                  placeholder="Phone Number"
                  id="name"
                  required
                />
                <label for="name" class="form__label">Phone Number</label>
              </div>

              <div class="form__group">
                <div class="form__radio-group">
                  <input
                    type="radio"
                    class="form__radio-input"
                    id="one"
                    name="time"
                  />
                  <label for="one" class="form__radio-label">
                    <span class="form__radio-button"></span> 1 Bulan
                  </label>
                </div>

                <div class="form__radio-group">
                  <input
                    type="radio"
                    class="form__radio-input"
                    id="six"
                    name="time"
                  />
                  <label for="six" class="form__radio-label">
                    <span class="form__radio-button"></span>6 Bulan
                  </label>
                </div>

                <div class="form__radio-group u-margin-bottom-medium">
                  <input
                    type="radio"
                    class="form__radio-input"
                    id="twelve"
                    name="time"
                  />
                  <label for="twelve" class="form__radio-label">
                    <span class="form__radio-button"></span>12 Bulan
                  </label>
                </div>
              </div>

              <div class="form__group">
                <button class="btn btn--green">Kirim &rarr;</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </main>
@endsection