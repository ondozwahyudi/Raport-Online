@extends('layouts.master')
@section('contents')
    <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-primary navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">          
        <a href=" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();"
          {{ __('Logout') }} >Logout</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>                
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src=" {{asset('/assets')}}/dist/img/Logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Raport Online</span>
    </a>
    @if (auth()->user()->role == 'guru')                   
    <div class='card'>
      <div class="card-body card-profile">
        <div class="text-center">
          <img class="profile-user-img img-responsive img-circle" src="{{auth()->user()->data_guru()->getPhoto()}}" alt="User profile picture">
        </div>
          <h3 class="profile-username text-center">{{auth()->user()->data_guru()->nama_guru}}</h3>
          <p class="text-muted text-center">Wali Kelas {{auth()->user()->data_guru()->kelas[0]->kelas_nama}} </p>            
        {{-- <a href="{{route('edit', $siswa->id)}}" class="btn btn-primary btn-block"><b>Edit</b></a> --}}
      </div>
    </div>
  <div class="sidebar">    
  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <li class="nav-item">
        <a href=" {{route('home_guru')}}" class="nav-link">
          <i class="nav-icon fa fa-dashboard"></i>
          <p>Dashboard</p>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.sidebar-menu -->
  </div>
@endif
    @if (auth()->user()->role == 'siswa')                   
      <div class='card'>
        <div class="card-body card-profile">
          <div class="text-center">
            <img class="profile-user-img img-responsive img-circle" src="{{auth()->user()->data_siswa()->getPhoto()}}" alt="User profile picture">
          </div>
            <h3 class="profile-username text-center">{{auth()->user()->data_siswa()->nama_siswa}}</h3>
            <p class="text-muted text-center">Kelas {{auth()->user()->data_siswa()->kelas->kelas_nama}} </p>            
          <a href="{{route('edit', $siswa->id)}}" class="btn btn-primary btn-block"><b>Edit</b></a>
        </div>
      </div>
    <div class="sidebar">    
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a href=" {{route('home')}}" class="nav-link">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>Dashboard</p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
  @endif
    <!-- Sidebar -->
    @if (auth()->user()->role == 'admin')
      <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src=" {{asset('/assets')}}/dist/img/default.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"> {{auth()->user()->name}} </a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href=" {{route('home_admin')}}" class="nav-link">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pie-chart"></i>
              <p>
                Data
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin_page_siswa') }}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Data Siswa</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin_page_guru') }}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Data Guru</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-files-o"></i>
              <p>
                Options
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin_page_mapel')}}" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                  <p>Mata Pelajaran</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin_page_kelas')}}" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Kelas</p>
                </a>
              </li>
            </ul>
            <li class="nav-item">
              <a href="pages/widgets.html" class="nav-link">
                <i class="nav-icon fa fa-th"></i>
                <p>
                  Widgets
                </p>
              </a>
            </li>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
      </div>
    @endif
    <!-- /.sidebar -->
  </aside>
  <div class="content-wrapper">
      @yield('contents_page')
  </div>
@endsection