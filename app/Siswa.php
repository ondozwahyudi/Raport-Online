<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = "siswa";
    protected $fillable = ['uuid','user_id','kelas_id','nis','nama_siswa','tanggal_lahir','upload_foto','agama','nama_ortu', 'alamat','pekerjaan','no_telp','jenis_kelamin'];
    // public $incrementing = false;

    // public function getRoutekeyName()
    // {
    //     return 'uuid';
    // }
    //funtion default images 
    public function getPhoto()
    {
        if(!$this->upload_foto){
            return (asset('assets/dist/img/default.png'));
        }
        return asset('images/'. $this->upload_foto);
    }
    //Relasi Table user
    public function user(){
        return User::where('id', $this->user_id)->first();
    }
    //relasi Table mapel
    public function mapel()
    {
        return $this->belongsToMany(Mapel::class)->withPivot(['nilai'])->withTimeStamps();
    }
    //relasi table kelas
    public function kelas()
    {
        return $this->belongsTo(Kelas::class);
    }
    //rata-rata
    public function rata_rata()
    {
        if($this->mapel->isNotEmpty()){
            $total = 0;
            $hasil = 0;
            foreach ($this->mapel as $mapel) {
                $total += $mapel->pivot->nilai;   
                $hasil++;
            }
        
        return round($total/$hasil);
        }
        return 0;
    }
}
