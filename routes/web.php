<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// sites web
Route::get('/', 'SiteController@index')->name('home_site');
Route::post('/send', 'SiteController@sendSms');
Route::get('/pesan_sekarang','SiteController@pesan')->name('pesan_sekarang_site');

Route::get('/price', 'SiteController@price')->name('price');
Route::get('/story', 'SiteController@story')->name('story');
Route::get('/about', 'SiteController@about')->name('about');
Route::get('/faq', 'SiteController@faq')->name('faq');
Route::get('/privacy', 'SiteController@privacy')->name('privacy');
Route::get('/login', 'SiteController@login')->name('login');


//api test
Route::get('/test','testuuid@index')->name('test_page');
Route::post('/test/add', 'testuuid@sendSms');
Route::get('/instagram','testuuid@getInstagram');

//admin site
Auth::routes(['verify'=> true]);
Route::group(['middleware' => ['auth','checkRole:admin']], function() {
        // Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/admin/dashboard', 'HomeController@index')->name('home_admin');
    Route::get('/admin/data_siswa', 'DataController@index_siswa')->name('admin_page_siswa');
    Route::post('/admin/add_action',[
        'uses'      => 'DataController@postInsertSiswa',
        'as'        => 'add_action'
    ]);
    Route::get('/admin/{uuid}/edit', [
        'uses'      => 'DataController@getEditSiswa',
        'as'        => 'edit_siswa'
    ]);
    Route::post('/admin/{id}/update',[
        'uses'      => 'DataController@getUpdateSiswa',
        'as'        => 'update_action'
    ]);
    Route::get('/admin/{id}/delete',[
        'uses'      => 'DataController@getDeleteSiswa',
        'as'        => 'delete_siswa'
    ]);


    Route::get('/admin/{id}/profil', 'DataController@index_profil')->name('page_profil');
    Route::get('/admin/data_guru', 'DataController@index_guru')->name('admin_page_guru');
    Route::post('/admin/add_guru', 'DataController@add_guru')->name('add_action_guru');
    Route::get('/admin/{id}/guru_edit', 'DataController@edit_guru')->name('edit_guru');
    Route::post('/admin/{id}/guru_update', 'DataController@update_guru')->name('get_update_guru');
    Route::get('/admin/{id}/guru_delete', 'DataController@delete_guru')->name('delete_guru');
//Route Mapel
    Route::get('/admin/mapel', 'MapelController@index')->name('admin_page_mapel');
    Route::post('admin/mapel_add', 'MapelController@addmapel')->name('add_mapel');
    Route::get('/admin/{id}/mapel_edit', 'MapelController@getedit')->name('get_edit_mapel');
    Route::post('/admin/{id}/mapel_update', 'MapelController@getupdate')->name('get_update_mapel');
    Route::get('/admin/{id}/mapel_delete','MapelController@getdelete')->name('mapel_delete');
//Route Pengumumman
    Route::get('/admin/pengumuman', 'PengumumanController@index')->name('admin_page_pengumuman');
//Route Kelas
    Route::get('/admin/kelas','KelasController@index')->name('admin_page_kelas');
    Route::get('/admin/{id}/kelas','KelasController@siswa_kelas')->name('page_siswa_kelas');
    Route::post('/admin/add_kelas','KelasController@add_kelas')->name('add_kelas');
    Route::get('/admin/{id}/kelas_edit','KelasController@getedit')->name('edit_kelas');
    Route::post('/admin/{id}/kelas/update','KelasController@getupdate')->name('get_update_kelas');
});

Route::group(['middleware' => ['auth','checkRole:siswa']], function() {
    Route::get('/siswa/dashboard', 'SiswaController@index')->name('home');
    // Route::get('/siswa/{id}/profile','SiswaController@edit_profile')->name('edit');
    Route::get('/siswa/nilai', 'SiswaController@nilai')->name('nilai');
});

Route::group(['middleware' => ['auth', 'checkRole:guru']], function(){
    Route::get('/guru/dashboard', 'GuruController@index')->name('home_guru');
    Route::get('/guru/{id}/profil', 'DataController@index_profil')->name('page_profil');
    Route::post('/guru/{id}/addnilai',[
        'uses'      => 'DataController@addnilai',
        'as'        =>'add_nilai'
    ]);
    Route::get('/guru/{id}/{idmapel}/deletenilai',[
        'uses'      => 'DataController@deletenilai',
        'as'        =>  'deletenilai'
    ]);

});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
