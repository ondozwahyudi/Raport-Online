
@extends('layouts.main_navigation')
@section('header')

@endsection
@section('contents_page')
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
          <div class="row mb-2">
          <div class="col-sm-6">
              <h1>Data Table</h1>
          </div>
          <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item active"><a href="#">Data Guru</a></li>
              <li class="breadcrumb-item active">Edit Data Guru</li>
              </ol>
          </div>
          </div>
      </div>
  </section>
        @if (session("success"))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;
                </span>
            </button>
  
            {{ session("success")}}
            <strong> Well done!
            </strong>
        </div>
        @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="box">
            <div class="modal-body">
                <form action="{{route('get_update_guru', $guru->id)}}" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('nama_guru') ? 'has-error' : '' }}">
                            <label for="nama_guru">Nama Guru</label>
                            {{-- auto nama_guru --}}
                            <input type="text" class="form-control" id="nama_guru" name="nama_guru" placeholder="nama_guru" value = "{{ $guru->nama_guru}}" autocomplete="off" required>
                            @if ($errors->has('nama_guru'))
                                <span class="help-block">{{$errors->first('nama_guru')}}</span>
                            @endif  
                        </div>
                        <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
                            <label for="alamat">Alamat</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" placeholder="alamat" value = "{{ $guru->alamat}}" autocomplete="off" required>
                            @if ($errors->has('alamat'))
                                <span class="help-block">{{$errors->first('alamat')}}</span>
                            @endif  
                        </div>
                        <div class="form-group {{ $errors->has('no_telp') ? 'has-error' : '' }}">
                                <label for="no_telp">No Telp</label>
                                <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="no_telp" value = "{{ $guru->no_telp}}" autocomplete="off" required>
                                @if ($errors->has('no_telp'))
                                    <span class="help-block">{{$errors->first('no_telp')}}</span>
                                @endif  
                            </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                </form>
                
            </div>
          </div>
        </div>
      </div>
    </section>
</div>
@endsection
@section('footer')


@endsection