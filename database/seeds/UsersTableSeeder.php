<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role' => 'admin',
                'name' => 'Gilang Wahyudi',
                'email' => 'ondozwahyudi@gmail.com',
                'email_verified_at' => '2019-06-24 17:11:15',
                'password' => '$2y$10$TcRF6ZIVwM4U2NA064x5iuGHE960QdXbEzi5n0E0m8qV5Nsj3/Ey.',
                'remember_token' => 'a1sZedc4zARZyQruWUl0umTWtWonz74mVH1QceDRyiubnZEyRtutzGNfLpKl',
                'created_at' => '2019-06-24 17:10:31',
                'updated_at' => '2019-06-25 06:02:06',
            ),
            1 => 
            array (
                'id' => 34,
                'role' => 'siswa',
                'name' => 'Gilang wahyudi',
                'email' => 'Gilang@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$dzmvN7MWTTPoOgQj9N9wWe3oKL8hFX34HiGKRNyjW89hPe1ZS1i1W',
                'remember_token' => 'RpZSCBdQGpDwlrkwgUL1YnIRrjCYwqUW1jEROZR6vDfV6EViIGnF3zB7gLfP',
                'created_at' => '2019-06-25 02:48:34',
                'updated_at' => '2019-06-26 02:20:50',
            ),
            2 => 
            array (
                'id' => 35,
                'role' => 'siswa',
                'name' => 'Afochar SR',
                'email' => 'Apenk@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$F9Y77y8peDZqbl5DYZNuI.VC3ZkbaBKZRl8yNN0WPDuNmm7hqYPWu',
                'remember_token' => 'LYNaVpeUtJlMtnTxEwfDIcl9G0B9umh49x1CUCACPAI00O5FX6kPSvW4I1dT',
                'created_at' => '2019-06-26 02:06:25',
                'updated_at' => '2019-06-28 15:37:44',
            ),
            3 => 
            array (
                'id' => 36,
                'role' => 'siswa',
                'name' => 'Arsanda Maulana',
                'email' => 'Arsa@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N1RvXVowtx3ee8ul2BArvuneW5WDU/q6EmWI7qHKRQDDWnBDN6qo.',
                'remember_token' => 'lGk8ZLagjP8IODTPEKNGfP3CUYom2EVI7wZkdp848T5zZhNGKImGUW4sLSlN',
                'created_at' => '2019-06-26 03:06:56',
                'updated_at' => '2019-06-26 03:06:56',
            ),
            4 => 
            array (
                'id' => 37,
                'role' => 'siswa',
                'name' => 'Dhimas Prayoga',
                'email' => 'Dhimas@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$I4c7M9kexUlC1EB4XCgFPuyTgjm2wHqKrzTnCuBACvrke7Qwt7J4y',
                'remember_token' => '9yYYRDqaGacfvioUqXq9xhnsorn9Q57Ito6AvS0Wcj7q7hIZhPiYd30K82Wq',
                'created_at' => '2019-06-26 07:54:29',
                'updated_at' => '2019-07-03 07:55:15',
            ),
            5 => 
            array (
                'id' => 38,
                'role' => 'siswa',
                'name' => 'Sidik Satiawan Yusuf',
                'email' => 'Sidik@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$BUdsktKsJ9a9ObywGtBGhO7m8ulnfzn./HpI4BO5R7BztnBiQAZcO',
                'remember_token' => 'VAyQjC8EIMvbsenqzGavf3W1q72W4weoI7kLMp6PJMFg9H8tPsp0hf6KZ6dT',
                'created_at' => '2019-06-26 13:05:36',
                'updated_at' => '2019-06-26 13:05:36',
            ),
            6 => 
            array (
                'id' => 40,
                'role' => 'siswa',
                'name' => 'Iqbal Maulna',
                'email' => 'Iqbal@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$uShjbfFW9qa3dZVXL9hvvOUYwPNDWDpyS8AUfTRucFnbjoiAgQaoe',
                'remember_token' => 'CBCjV8evssHkj4R0MOPIQ0JkLq5HFWMdyqUedNQhUbFuwelOq2vxckbws22v',
                'created_at' => '2019-06-29 06:55:05',
                'updated_at' => '2019-06-29 06:55:05',
            ),
            7 => 
            array (
                'id' => 41,
                'role' => 'siswa',
                'name' => 'Muhammad Fatir Al-Farizi',
                'email' => 'Fatir@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$YIPTbL82FAn4BLiR3QICfu5p5Qj29z2m2fK9OGbnijLfwJuZOCQQC',
                'remember_token' => 'hPf5wysQyFtjmuogU5A96390AH7GBJM4avhoftOqSXJVqGME2K2q2e1u6L7F',
                'created_at' => '2019-07-02 12:09:55',
                'updated_at' => '2019-07-02 12:09:55',
            ),
            8 => 
            array (
                'id' => 42,
                'role' => 'guru',
                'name' => 'Herman Agustin',
                'email' => 'Herman@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$PrvU1Z4XdhBkDUXDN1jbk.TNPsU0eQPqlaD9cMuPF32tebcW1N3KC',
                'remember_token' => 'f9H0DxiJIR8gVBBiajhYxlM70lmss4IZ8RhYPuxyN7RMnrJ4XSUsvBr8OMUH',
                'created_at' => '2019-07-03 11:18:02',
                'updated_at' => '2019-07-03 11:18:02',
            ),
            9 => 
            array (
                'id' => 43,
                'role' => 'guru',
                'name' => 'Suharno',
                'email' => 'Suharno@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$BGq4QikPK08ixP0Dbm9odeCaGLWH8VP2DdPynmJ4XdM1Y4oCApL7C',
                'remember_token' => 'Fu2FgQjK71slyjKZH6afKw0lpaV2C2Gmw9h5P06xvDha4YqB6KmhMpR5zWfy',
                'created_at' => '2019-07-03 11:41:49',
                'updated_at' => '2019-07-03 11:41:49',
            ),
            10 => 
            array (
                'id' => 44,
                'role' => 'guru',
                'name' => 'Kandoyo',
                'email' => 'Kandoyo@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$RfMOs/25N4s6eIeN73tru.N5604P.NVBUXMfcse.vkixUuLryL5UG',
                'remember_token' => 'NcvqHEsT3UxNWMRgzJCFY87VNMHxeLBDKW3zwEHsFlnSIlhAfkeqUfof4Pkk',
                'created_at' => '2019-07-03 11:42:22',
                'updated_at' => '2019-07-03 11:42:22',
            ),
            11 => 
            array (
                'id' => 45,
                'role' => 'guru',
                'name' => 'Firmasah',
                'email' => 'Firmansah@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$BTMVC9TSfGpAo8qYVkvn9.pqNMLD42v3QXayCR0ecKFcHMwjqqGjC',
                'remember_token' => 'yYzypdT0HVBlDL9oucPosiQtIRA4UbI6OMl1srhpMXAPzivQ83ZxNBuQWftX',
                'created_at' => '2019-07-03 11:43:04',
                'updated_at' => '2019-07-03 11:43:04',
            ),
            12 => 
            array (
                'id' => 46,
                'role' => 'guru',
                'name' => 'Rusmanto',
                'email' => 'Rusmanto@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$KWAKl4sfVveJpfaodlWkZOo/o9.PPh7vIMYujWntL9v.IHy1ZlV6e',
                'remember_token' => 'BUv7d1BYxi26yT30njTtm9WCCy70icjWMBohm27PjTTBP2IEx52FAnOZxebN',
                'created_at' => '2019-07-03 11:43:46',
                'updated_at' => '2019-07-03 11:43:46',
            ),
            13 => 
            array (
                'id' => 47,
                'role' => 'guru',
                'name' => 'Sri Martini',
                'email' => 'Sri@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$wPeLWKJJzqyDWvL3/us1LeAp89f1ot9ZwnvGUNdHdjm8Vgma5BWl6',
                'remember_token' => '46QQKqBEY1UoFi7Ascl6TlUnryLz3fX6Y8lgR0rQKX8u5Sz0FZzh6UO5swEj',
                'created_at' => '2019-07-03 11:44:39',
                'updated_at' => '2019-07-03 11:44:39',
            ),
            14 => 
            array (
                'id' => 48,
                'role' => 'guru',
                'name' => 'Sri Ningsih',
                'email' => 'ning@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$FktkRw3EuABdr8AvsXhJ1O6gEiP8zMawuYSI7hzzX3S7XCQ1i9Stm',
                'remember_token' => 'ipgSoXOl2IPrgMo2pAB52aYwqXr8bOEwuT1w1y8ZSlR3qJHqeG4VlJWCY9BT',
                'created_at' => '2019-07-03 11:45:41',
                'updated_at' => '2019-07-03 11:45:41',
            ),
            15 => 
            array (
                'id' => 49,
                'role' => 'guru',
                'name' => 'Subono',
                'email' => 'Subono@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$0RNkjHLG/Jrfsv7/T/4lAek9Z//QyzFrFyGE0vjP8JWuF8KFP8VHy',
                'remember_token' => 'ZQhm0aTD9eWeLvdh7yEmWBmcRjoTfKEZPc2Ej5lZTTOMvUxuo5st52ObdtWI',
                'created_at' => '2019-07-03 11:46:20',
                'updated_at' => '2019-07-03 11:46:20',
            ),
            16 => 
            array (
                'id' => 50,
                'role' => 'guru',
                'name' => 'Maksum',
                'email' => 'Maksum@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$NqclgOgskXvwnr4uWSnsoOMeuXFk3pr28E8MzENNGEopqolP0LqAG',
                'remember_token' => 'QBUfWVAmgiXhj6bDAH6pTFMZTsxVTGHG12cMpIxvBgo7XcMFBfg2fufgEqrO',
                'created_at' => '2019-07-03 11:46:50',
                'updated_at' => '2019-07-03 11:46:50',
            ),
            17 => 
            array (
                'id' => 51,
                'role' => 'guru',
                'name' => 'Tugiar',
                'email' => 'Tugiar@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$jUXeDaZeRkAng25Nmt89BeTAjN2EyVEJDTP6xRv9n6mYYDxsjEa3e',
                'remember_token' => '4JRx91FBqtUbgzx5GHX49NHLJRwbk7s4t9IOFQVnqdOvTfK0fEB976eMJ2Vp',
                'created_at' => '2019-07-03 11:47:34',
                'updated_at' => '2019-07-03 11:47:34',
            ),
        ));
        
        
    }
}