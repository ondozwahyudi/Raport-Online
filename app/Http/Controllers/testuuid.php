<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facedes\Session;
use Vinkla\Instagram\Instagram;
use Nexmo\Laravel\Facade\Nexmo;

class testuuid extends Controller
{
    // public function index_ginerate()
    // {
    //     $uuid4 = Uuid::uuid4();
    //     $datauuid = $uuid4->toString() . "1";
    //     return response()->json($datauuid);
    // }

    public function index()
    {
        $instagram = new Instagram('2130148879.212a4a9.a88b3733c8464f59ac037b55190d67f4');
        // $data  = $instagram->self();
        $image = $instagram->media(['count' => 2]);
        $images = [];
        foreach ($image as $data) {
            $images[] = $data->images->standard_resolution->url;
        }
        // return response()->json([$photo]);
        return view('test',[ 'images' => $images]); 
    }

    public function sendSms(Request $request)
    {
        Nexmo::message()->send([
            'to'   => '62'.$request->mobile,
            'from' => 'NEXMO',
            'text' => $request->masukan,
        ]);
        // Session::flash('success', 'Berhasil Mengirim');
        return redirect()->route('test_page')->with('success', 'Berhasil terkirim');;
    }
    public function getInstagram()
    {
        // 2130148879.212a4a9.a88b3733c8464f59ac037b55190d67f4
        $instagram = new Instagram('2130148879.212a4a9.a88b3733c8464f59ac037b55190d67f4');
        // $data  = $instagram->self('');
        $image = $instagram->media(['count' => 2]);
        $images = [];
        foreach ($image as $data) {
            $images[] = $data;
        }
        return response()->json([$images]);
    }
}
