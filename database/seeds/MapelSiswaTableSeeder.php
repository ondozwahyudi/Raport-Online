<?php

use Illuminate\Database\Seeder;

class MapelSiswaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mapel_siswa')->delete();
        
        \DB::table('mapel_siswa')->insert(array (
            0 => 
            array (
                'id' => 28,
                'mapel_id' => 7,
                'siswa_id' => 17,
                'nilai' => 99,
                'created_at' => '2019-06-28 14:59:06',
                'updated_at' => '2019-06-28 14:59:06',
            ),
            1 => 
            array (
                'id' => 29,
                'mapel_id' => 8,
                'siswa_id' => 17,
                'nilai' => 88,
                'created_at' => '2019-06-28 15:34:42',
                'updated_at' => '2019-06-28 15:34:42',
            ),
            2 => 
            array (
                'id' => 30,
                'mapel_id' => 9,
                'siswa_id' => 17,
                'nilai' => 99,
                'created_at' => '2019-06-29 08:23:51',
                'updated_at' => '2019-06-29 08:23:51',
            ),
            3 => 
            array (
                'id' => 31,
                'mapel_id' => 10,
                'siswa_id' => 17,
                'nilai' => 77,
                'created_at' => '2019-06-29 08:23:57',
                'updated_at' => '2019-06-29 08:23:57',
            ),
            4 => 
            array (
                'id' => 32,
                'mapel_id' => 11,
                'siswa_id' => 17,
                'nilai' => 76,
                'created_at' => '2019-06-29 08:24:03',
                'updated_at' => '2019-06-29 08:24:03',
            ),
            5 => 
            array (
                'id' => 33,
                'mapel_id' => 12,
                'siswa_id' => 17,
                'nilai' => 78,
                'created_at' => '2019-06-29 08:24:10',
                'updated_at' => '2019-06-29 08:24:10',
            ),
            6 => 
            array (
                'id' => 34,
                'mapel_id' => 7,
                'siswa_id' => 18,
                'nilai' => 88,
                'created_at' => '2019-06-29 08:24:30',
                'updated_at' => '2019-06-29 08:24:30',
            ),
            7 => 
            array (
                'id' => 35,
                'mapel_id' => 8,
                'siswa_id' => 18,
                'nilai' => 92,
                'created_at' => '2019-06-29 08:24:41',
                'updated_at' => '2019-06-29 08:24:41',
            ),
            8 => 
            array (
                'id' => 36,
                'mapel_id' => 9,
                'siswa_id' => 18,
                'nilai' => 90,
                'created_at' => '2019-06-29 08:24:51',
                'updated_at' => '2019-06-29 08:24:51',
            ),
            9 => 
            array (
                'id' => 37,
                'mapel_id' => 10,
                'siswa_id' => 18,
                'nilai' => 67,
                'created_at' => '2019-06-29 08:25:02',
                'updated_at' => '2019-06-29 08:25:02',
            ),
            10 => 
            array (
                'id' => 38,
                'mapel_id' => 11,
                'siswa_id' => 18,
                'nilai' => 99,
                'created_at' => '2019-06-29 08:25:07',
                'updated_at' => '2019-06-29 08:25:07',
            ),
            11 => 
            array (
                'id' => 39,
                'mapel_id' => 12,
                'siswa_id' => 18,
                'nilai' => 78,
                'created_at' => '2019-06-29 08:25:14',
                'updated_at' => '2019-06-29 08:25:14',
            ),
            12 => 
            array (
                'id' => 40,
                'mapel_id' => 8,
                'siswa_id' => 19,
                'nilai' => 88,
                'created_at' => '2019-06-29 08:25:30',
                'updated_at' => '2019-07-02 12:49:37',
            ),
            13 => 
            array (
                'id' => 41,
                'mapel_id' => 7,
                'siswa_id' => 19,
                'nilai' => 87,
                'created_at' => '2019-06-29 08:25:35',
                'updated_at' => '2019-06-29 08:25:35',
            ),
            14 => 
            array (
                'id' => 42,
                'mapel_id' => 9,
                'siswa_id' => 19,
                'nilai' => 87,
                'created_at' => '2019-06-29 08:25:40',
                'updated_at' => '2019-06-29 08:25:40',
            ),
            15 => 
            array (
                'id' => 43,
                'mapel_id' => 10,
                'siswa_id' => 19,
                'nilai' => 77,
                'created_at' => '2019-06-29 08:25:48',
                'updated_at' => '2019-06-29 08:25:48',
            ),
            16 => 
            array (
                'id' => 44,
                'mapel_id' => 11,
                'siswa_id' => 19,
                'nilai' => 83,
                'created_at' => '2019-06-29 08:25:55',
                'updated_at' => '2019-06-29 08:25:55',
            ),
            17 => 
            array (
                'id' => 45,
                'mapel_id' => 12,
                'siswa_id' => 19,
                'nilai' => 45,
                'created_at' => '2019-06-29 08:26:01',
                'updated_at' => '2019-07-02 12:45:16',
            ),
            18 => 
            array (
                'id' => 46,
                'mapel_id' => 7,
                'siswa_id' => 20,
                'nilai' => 98,
                'created_at' => '2019-06-29 08:26:20',
                'updated_at' => '2019-06-29 08:26:20',
            ),
            19 => 
            array (
                'id' => 47,
                'mapel_id' => 8,
                'siswa_id' => 20,
                'nilai' => 88,
                'created_at' => '2019-06-29 08:26:25',
                'updated_at' => '2019-06-29 08:26:25',
            ),
            20 => 
            array (
                'id' => 48,
                'mapel_id' => 9,
                'siswa_id' => 20,
                'nilai' => 78,
                'created_at' => '2019-06-29 08:26:31',
                'updated_at' => '2019-06-29 08:26:31',
            ),
            21 => 
            array (
                'id' => 49,
                'mapel_id' => 10,
                'siswa_id' => 20,
                'nilai' => 99,
                'created_at' => '2019-06-29 08:26:35',
                'updated_at' => '2019-06-29 08:26:35',
            ),
            22 => 
            array (
                'id' => 50,
                'mapel_id' => 11,
                'siswa_id' => 20,
                'nilai' => 76,
                'created_at' => '2019-06-29 08:26:41',
                'updated_at' => '2019-06-29 08:26:46',
            ),
            23 => 
            array (
                'id' => 51,
                'mapel_id' => 12,
                'siswa_id' => 20,
                'nilai' => 82,
                'created_at' => '2019-06-29 08:26:55',
                'updated_at' => '2019-06-29 08:26:55',
            ),
            24 => 
            array (
                'id' => 52,
                'mapel_id' => 7,
                'siswa_id' => 21,
                'nilai' => 99,
                'created_at' => '2019-06-29 08:27:19',
                'updated_at' => '2019-06-29 08:27:19',
            ),
            25 => 
            array (
                'id' => 53,
                'mapel_id' => 8,
                'siswa_id' => 21,
                'nilai' => 98,
                'created_at' => '2019-06-29 08:27:26',
                'updated_at' => '2019-06-29 08:27:26',
            ),
            26 => 
            array (
                'id' => 54,
                'mapel_id' => 9,
                'siswa_id' => 21,
                'nilai' => 96,
                'created_at' => '2019-06-29 08:27:33',
                'updated_at' => '2019-06-29 08:27:33',
            ),
            27 => 
            array (
                'id' => 55,
                'mapel_id' => 10,
                'siswa_id' => 21,
                'nilai' => 91,
                'created_at' => '2019-06-29 08:27:40',
                'updated_at' => '2019-06-29 08:27:40',
            ),
            28 => 
            array (
                'id' => 56,
                'mapel_id' => 11,
                'siswa_id' => 21,
                'nilai' => 88,
                'created_at' => '2019-06-29 08:27:47',
                'updated_at' => '2019-06-29 08:27:47',
            ),
            29 => 
            array (
                'id' => 57,
                'mapel_id' => 12,
                'siswa_id' => 21,
                'nilai' => 92,
                'created_at' => '2019-06-29 08:27:53',
                'updated_at' => '2019-06-29 08:27:53',
            ),
            30 => 
            array (
                'id' => 58,
                'mapel_id' => 7,
                'siswa_id' => 22,
                'nilai' => 13,
                'created_at' => '2019-06-29 08:38:25',
                'updated_at' => '2019-06-29 10:25:26',
            ),
            31 => 
            array (
                'id' => 59,
                'mapel_id' => 8,
                'siswa_id' => 22,
                'nilai' => 100,
                'created_at' => '2019-06-29 08:38:30',
                'updated_at' => '2019-06-29 08:38:30',
            ),
            32 => 
            array (
                'id' => 60,
                'mapel_id' => 9,
                'siswa_id' => 22,
                'nilai' => 100,
                'created_at' => '2019-06-29 08:38:35',
                'updated_at' => '2019-06-29 08:38:35',
            ),
            33 => 
            array (
                'id' => 61,
                'mapel_id' => 10,
                'siswa_id' => 22,
                'nilai' => 100,
                'created_at' => '2019-06-29 08:38:39',
                'updated_at' => '2019-06-29 08:38:39',
            ),
            34 => 
            array (
                'id' => 62,
                'mapel_id' => 11,
                'siswa_id' => 22,
                'nilai' => 100,
                'created_at' => '2019-06-29 08:38:45',
                'updated_at' => '2019-06-29 08:38:45',
            ),
            35 => 
            array (
                'id' => 63,
                'mapel_id' => 12,
                'siswa_id' => 22,
                'nilai' => 90,
                'created_at' => '2019-06-29 08:38:51',
                'updated_at' => '2019-06-29 08:38:51',
            ),
            36 => 
            array (
                'id' => 64,
                'mapel_id' => 13,
                'siswa_id' => 21,
                'nilai' => 88,
                'created_at' => '2019-07-03 11:34:59',
                'updated_at' => '2019-07-03 11:34:59',
            ),
            37 => 
            array (
                'id' => 65,
                'mapel_id' => 13,
                'siswa_id' => 18,
                'nilai' => 88,
                'created_at' => '2019-07-03 13:24:28',
                'updated_at' => '2019-07-03 13:24:28',
            ),
        ));
        
        
    }
}