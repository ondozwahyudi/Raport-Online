@extends('layouts.frontend')
@section('contents')
    <header>
      <div class="section-login">
        <div class="row">
          <div class="book">
            <div class="book__form">
              <form method="POST" action="{{ route('login') }}">
                  @csrf
                <div class="u-margin-bottom-medium">
                  <h2 class="heading-secondary">
                    Masuk Ke AKun Anda
                  </h2>
                </div>

                <div class="form__group">
                  <input
                    type="email"
                    class="form__input @error('email') is-invalid @enderror"
                    placeholder="Masukkan E-mail"
                    name="email" 
                    value="{{ old('email') }}"
                    id="email"
                    autofocus
                    required
                  />
                  <label for="email" class="form__label">Masukkan E-mail</label>
                  @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>

                <div class="form__group">
                  <input
                    type="password"
                    class="form__input @error('password') is-invalid @enderror"
                    name="password"
                    placeholder="Masukkan Password"
                    id="password"
                    autocomplete="current-password"
                    required
                  />
                  <label for="password" class="form__label"
                    >Masukkan Password</label
                  >
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>

                <div class="form__group">
                  <button class="btn btn--green" type="submit">{{ __('Login') }}</button>
                  @if (Route::has('password.request'))
                      <a style="margin-left:13rem;text-decoration:none;color:gray" class="" href="{{ route('password.request') }}">
                          {{ __('Forgot Your Password?') }}
                      </a>
                  @endif
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </header>
@endsection