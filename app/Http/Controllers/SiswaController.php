<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\User;
use App\Mapel;
use App\Guru;
use App\Kelas;
use Illuminate\Support\Facades\Crypt;

class SiswaController extends Controller

{
    public function index()
    {
        $siswa  = auth()->user()->data_siswa();

        // return response()->json($data->nama_guru);
        return view('siswa.index', ['siswa' => $siswa]);
    }
    public function nilai()
    {
        return view('siswa.page_nilai');
    }

    public function edit_profile(Request $request,$id)
    {
        $siswa = Siswa::find($id);
        return response()->json($siswa);
        // return view('siswa.edit_profile');
    }
}
