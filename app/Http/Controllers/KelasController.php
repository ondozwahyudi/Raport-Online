<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\User;
use App\Mapel;
use App\Guru;
use App\Kelas;

class KelasController extends Controller
{
    public function index(){
        $data_guru = Guru::all();
        $kelas = Kelas::all();
        $data_siswa = Siswa::all();
        // return response()->json($data_guru);
        return view('admin.kelas.page_kelas',['kelas' => $kelas, 'data_guru' => $data_guru, 'data_siswa' => $data_siswa]);
    }

    public function siswa_kelas($id){
        $kelas = Kelas::find($id);
        $data = $kelas->siswa->map(function($s){
            $s->rata_rata = $s->rata_rata();
            return $s;
        });
        $kelas = $data->sortByDesc('rata_rata');
        return view('admin.kelas.page_siswa_kelas',['kelas' => $kelas]);
    }

    public function add_kelas(Request $request)
    {
        $kelas = Kelas::create($request->all());
        return redirect()->route('admin_page_kelas')->with('success', 'You seccessfully add');
    }

    public function getedit($id)
    {
        $kelas = kelas::find($id);
        $data_guru = Guru::all();
        return view('admin.kelas.page_edit_kelas',['kelas'=>$kelas, 'data_guru' => $data_guru]);
    }

    public function getupdate(Request $request,$id){

        $mapel = Kelas::find($id);
        $mapel->update($request->all());
        return redirect()->route('admin_page_kelas')->with('success', ' You successfully Update');
    }
}
