<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\User;
use App\Mapel;
use App\Guru;
use App\Kelas;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

// use App\Kelas;

class DataController extends Controller
{
    //fucntion page data table siswa
    public function index_siswa(Request $request)
    {

        $data_siswa = Siswa::all();
        $data_kelas = Kelas::all();

        $data_siswa->map(function($s){
            $s->rata_rata = $s->rata_rata();
            return $s;
        });
        $data_siswa = $data_siswa->sortByDesc('rata_rata');

            // dd($data_siswa);
        return view('admin.data.page_data_siswa',['data_siswa' => $data_siswa,'data_kelas' => $data_kelas])->with('success', 'You successfully add');

    }

    public function postInsertSiswa(Request $request)
    {
    //it's required
        $this->validate($request,[
            'nis'           => 'required',
            'nama_siswa'    => 'required',
            'kelas_id'      => 'required',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required',
            'agama'         => 'required',
            'email'         => 'required|email|unique:users',
            'nama_ortu'     => 'required',
            'pekerjaan'     => 'required',
            'alamat'        => 'required',
            'no_telp'       => 'required',
        ]);
    //insert data table user
        $user = new User();
        $user->name             = $request->nama_siswa;
        $user->role             = 'siswa';
        $user->email            = $request->email;
        $user->password         = bcrypt('v113u5gw');
        $user->remember_token   = str_random(60);
        $user->save();

    //insert data table kelas

        $request->request->add(['user_id' => $user->id]);
        // $request['uuid'] = Uuid::uuid4()->toString(). $request->nis;
        $siswa = Siswa::create($request->all());
        // Siswa::create($request->all())
        return redirect()->route('admin_page_siswa')->with('success', 'You successfully add');
    }
    //get edit data siswa
    public function getEditSiswa($uuid){
        $siswa = Siswa::find($uuid);
        // return response()->json($siswa);
        $kelas = Kelas::all();
        return view('admin.data.page_data_edit_siswa', ['siswa'=> $siswa,'kelas' => $kelas])->with('success', 'You successfully update');
    }
    //get update datasiswa
    public function getUpdateSiswa(Request $request,$id)
    {
        // dd($request->all());
        $siswa = Siswa::find($id);
        $user = User::find($siswa->user_id);
        $user->email = $request->email;
        $user->save();
        $siswa->update($request->all());
        if($request->hasfile('upload_foto')){
            $upload_photo  = $request->file('upload_foto');
            $filename = time() . '.' . $upload_photo->getClientOriginalExtension();
            Image::make($upload_photo)->resize(350, 350)->save( public_path('images/'. $filename));
            $siswa->upload_foto = $filename;
            $siswa->save();
        // $request->file('upload_foto')->move('images/', $request->file('upload_foto')->getClientOriginalName());
        // $siswa->upload_foto = $request->file('upload_foto')->getClientOriginalName();
        }
        return redirect()->route('admin_page_siswa')->with('success', ' You successfully Update');
    }
    //get delete siswa
    public function getDeleteSiswa($id){
        $siswa = Siswa::find($id);
        $siswa->delete();
        return redirect()->back()->with('success', 'You successfully add');
    }
    //get Profil Siswa
    public function index_profil($id)
    {
        $siswa = Siswa::find($id);
        $mapell = Mapel::all();
        foreach($siswa->kelas as $data_kelas)
        {
         $kelas = $data_kelas;
        }
         $categories = [];
         $data_nilai = [];
         foreach($mapell as $mp){
             if($siswa->mapel()->wherePivot('mapel_id', $mp->id)->first()){
                 $categories[] = $mp->mapel;
                 $data_nilai[] = $siswa->mapel()->wherePivot('mapel_id', $mp->id)->first()->pivot->nilai;
             }
         }
        return view('admin.data.page_data_profil', ['siswa' => $siswa, 'mapell' => $mapell, 'categories' => $categories, 'data_nilai' => $data_nilai, 'kelas' => $kelas]);
    }
    // get add nilai
    public function addnilai(Request $request,$idsiswa)
    {
        $siswa = Siswa::find($idsiswa);
        if($siswa->mapel()->where('mapel_id', $request->mapel)->exists()){
            return redirect()->route('page_profil', $idsiswa)->with('error', 'data nilai sudah ada');
        }
        $siswa->mapel()->attach($request->mapel, ['nilai' => $request->nilai]);
        return redirect()->route('page_profil', $idsiswa)->with('success', 'You successfully');
    }
    //edit data nilai
    public function editnilai(Request $request, $id)
    {
        $siswa = Siswa::find($id);
        $siswa->mapel()->updateExistingPivot($request->pk,['nilai' => $request->value]);
        return redirect()->back();
    }
    //delete data nilai
    public function deletenilai($idsiswa, $idmapel)
    {
        $siswa = Siswa::find($idsiswa);
        $siswa->mapel()->detach($idmapel);
        return redirect()->back()->with('success',' Delete You successfully');
    }
    //function page data table guru
    public function index_guru()
    {
        $data_guru = Guru::all();
        return view('admin.data.page_data_guru', ['data_guru'=> $data_guru]);
    }
    //function add data guru
    public function add_guru(Request $request)
    {
        $this->validate($request,[
            'nama_guru'     => 'required',
            'email'         => 'required|email|unique:users',
            'alamat'        => 'required',
            'no_telp'       => 'required',
        ]);
         //insert data table user
         $user = new User();
         $user->name             = $request->nama_guru;
         $user->role             = 'guru';
         $user->email            = $request->email;
         $user->password         = bcrypt('password');
         $user->remember_token   = str_random(60);
         $user->save();

     //insert data table kelas
         $request->request->add(['user_id' => $user->id]);

        $guru = Guru::create($request->all());
        return redirect()->route('admin_page_guru')->with('success', 'You successfully add');
    }
    public function edit_guru(Request $request, $id)
    {
        $guru = Guru::find($id);
        return view('admin.data.page_data_edit_guru',['guru' => $guru])->with('success', 'You successfully edit');
    }
    public function update_guru(Request $request,$id)
    {
        $guru = Guru::find($id);
        $guru->update($request->all());
        return redirect()->route('admin_page_guru')->with('success', ' You successfully Update');
    }
     //get delete siswa
     public function getDeleteGuru($id){
        $siswa = Guru::find($id);
        $siswa->delete();
        return redirect()->back()->with('success', 'You successfully add');
    }

}
