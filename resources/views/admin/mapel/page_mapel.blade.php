@extends('layouts.main_navigation')
@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('assets/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection
@section('contents_page')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Data Mapel</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Data Mapel</li>
              </ol>
            </div>
          </div>
        </div>
    </section>
      @if (session("success"))
      <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;
              </span>
          </button>

          {{ session("success")}}
          <strong> Well done!
          </strong>
      </div>
      @endif
     <!-- Main content -->
     <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
              <h3 class="card-title">Data Table With Full Features</h3>
              <div style="
                float:right;
                position: absolute;
                top: 15px;
                right: 10px;">
                  <!-- Button trigger modal -->
                  <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Tambah Data
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">
                  <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr>
                          <th>Kode</th>
                          <th>Mata Pelajaran</th>                  
                          <th>Guru</th>        
                          <th>Aksi</th>
                        </tr>
                        </thead>
                        
                        <tbody>
                        @foreach ($mapel as $mp)
                        <tr>
                          <td> {{ $mp->kode }}</td>
                          <td> {{ $mp->mapel}} </td>
                          <td> {{ $mp->guru['nama_guru']}} </td>             
                          <td>
                            <a href="{{route("get_edit_mapel",$mp->id )}}" class="btn btn-sm btn-warning" ><i class="fa fa-pencil-square"></i></a>
                            <a href="{{ route('mapel_delete', [$mp->id])}}" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody> 
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Mapel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="{{route('add_mapel')}}" method="POST">
              <div class="box-body">
                <div class="form-group {{ $errors->has('kode') ? 'has-error' : '' }}">
                  <label for="kode">Kode Mata Pelajaran</label>
                  {{-- auto kode --}}
                  <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Mata Pelajaran"
                    value = ""
                    autocomplete="off" required>
                  @if ($errors->has('kode'))
                    <span class="help-block">{{$errors->first('kode')}}</span>
                  @endif  
                </div>
                <div class="form-group {{ $errors->has('mapel') ? 'has-error' : ''}}">
                  <label for="mapel">Mata Pelajaran</label>
                <input type="text" class="form-control" id="mapel" value="{{old('mapel')}}" name="mapel" placeholder="Mata Pelajaran" autocomplete="off" required>
                  @if ($errors->has('mapel'))
                    <span class="help-block">{{$errors->first('mapel')}}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('guru_id') ? 'has-error' : ''}}" >
                  <label for="guru">Pilih Guru</label>
                  <select name="guru_id" id="guru"  class="form-control" required>
                    <option value="">Pilih Guru</option>
                    @foreach ($data_guru as $guru)
                      <option value="{{$guru->id}}">{{$guru->nama_guru}} </option>
                    @endforeach
                  </select>
                  @if ($errors->has('guru_id'))
                    <span class="help-block">{{$errors->first('guru_id')}}</span>
                  @endif
                </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <input type="hidden" value="{{ Session::token() }}" name="_token">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer')
<!-- DataTables -->
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.bootstrap4.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    // $('#example1').DataTable()
    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  })

</script>

@endsection