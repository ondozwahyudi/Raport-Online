
@extends('layouts.main_navigation')
@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('contents_page')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Data Kelas</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Data</a></li>
                <li class="breadcrumb-item active">Data Kelas</li>
              </ol>
            </div>
          </div>
        </div>
    </section>

    <!-- Main content -->
    <div class="col-6">
        <div style="overflow-x:auto;">
          <div class="card card-primary card-outline" ">
            <table id="example1" class="table table-striped">
              <tr style="background:#3c8dbc;">
                <th>Nim</th>
                <th>Nama Siswa</th>
                <th>Nilai rata-rata</th>
                <th>Rangking</th>
              
              </tr>
              @php
              $rangking = 1;    
              @endphp
              @foreach ($kelas as $kelas_siswa)
              <tr>  
                <td style="width:100px">{{$kelas_siswa->nis}}</td>
                <td style="width:300px">{{$kelas_siswa->nama_siswa}}</td>
                <td style="width:200px">{{$kelas_siswa->rata_rata()}}</td>
                <td> {{$rangking}}</td>                      
              </tr>
              @php
              $rangking++;    
              @endphp
              @endforeach
            </table>
          </div>
        </div>
      </div>
    <!-- /.content -->
<!-- ./wrapper -->
@endsection
@section('footer')
<!-- DataTables -->
<script src="{{asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- page script -->
<script>
        $(function () {
          $('#example1').DataTable()
          $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
          })
        })
      </script>
@endsection



