<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('admin/{id}/editnilai', 'DataController@editnilai')->name('editnilai');
Route::get('/siswa/{id}/profile','SiswaController@edit_profile')->name('edit');
// Route::url('https://api.instagram.com/v1/users/self/?access_token=2130148879.212a4a9.a88b3733c8464f59ac037b55190d67f4')
