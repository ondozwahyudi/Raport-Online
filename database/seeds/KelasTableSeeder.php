<?php

use Illuminate\Database\Seeder;

class KelasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kelas')->delete();
        
        \DB::table('kelas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'guru_id' => 0,
                'kelas_nama' => 'X ',
                'created_at' => NULL,
                'updated_at' => '2019-07-03 12:10:55',
            ),
            1 => 
            array (
                'id' => 2,
                'guru_id' => 0,
                'kelas_nama' => 'XI IPA',
                'created_at' => NULL,
                'updated_at' => '2019-07-03 12:11:03',
            ),
            2 => 
            array (
                'id' => 3,
                'guru_id' => 0,
                'kelas_nama' => 'XII IPA ',
                'created_at' => NULL,
                'updated_at' => '2019-07-03 12:11:15',
            ),
            3 => 
            array (
                'id' => 4,
                'guru_id' => 0,
                'kelas_nama' => 'XI IPS ',
                'created_at' => '2019-06-29 12:19:03',
                'updated_at' => '2019-07-03 12:11:09',
            ),
            4 => 
            array (
                'id' => 5,
                'guru_id' => 0,
                'kelas_nama' => 'XII IPS ',
                'created_at' => '2019-06-29 12:19:37',
                'updated_at' => '2019-07-03 12:11:21',
            ),
        ));
        
        
    }
}