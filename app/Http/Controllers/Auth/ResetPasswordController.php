<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
     // protected $redirectTo = '/siswa';
     public function redirectTo(){
        // User role
        $role = auth()->user()->role; 
        
        // Check user role
        switch ($role) {
            case 'admin':
                    return '/admin/dashboard';
                break;
            case 'siswa':
                    return '/siswa/dashboard';
                break;
            case 'guru':
                    return '/guru/dashboard';
                break; 
            default:
                    return '/login'; 
                break;
        }
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
