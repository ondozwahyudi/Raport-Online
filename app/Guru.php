<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = "guru";
    protected $fillable = ['nama_guru','alamat','no_telp','user_id', 'upload_foto'];

    public function mapel()
    {
        return $this->hasMany(Mapel::class);
    }

    public function user()
    {
        return User::where('id', $this->user_id)->first();
    }
    public function kelas()
    {
        return $this->hasMany(Kelas::class);        
    }
    
    public function siswa()
    {
        return $this->hasMany(Siswa::class)->first();
    }
      //funtion default images 
    public function getPhoto()
    {
        if(!$this->upload_foto){
            return (asset('assets/dist/img/default.png'));
        }
        return asset('images/'. $this->upload_foto);
    }
}
