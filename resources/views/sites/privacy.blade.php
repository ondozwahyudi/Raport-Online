@extends('layouts.frontend')
@section('contents')
    <header class="header">
      <div class="header__logo-box">
        <img src=" {{asset('/frontend')}}/img/lapomark.png" alt="Logo" class="header__logo" />
      </div>

      <div class="header__text-box">
        <h1 class="heading-primary">
          <span class="heading-primary--large">Privacy & Policy</span>
        </h1>
        <p class="paragraph paragraph--white">
          Berikut aturan-aturan yang kami tentukan dalam aplikasi kami.
        </p>

        <a href="#privacy" class="btn btn--white btn--animated">Lihat Aturan</a>
      </div>
    </header>

    <main>
      <section class="section-privacy" id="privacy">
        <div class="u-center-text ">
          <h2 class="heading-secondary u-margin-bottom-small">
            Kebijakan Privasi untuk LapoMark
          </h2>
        </div>

        <div class="row">
          <p class="paragraph">
            Di LapoMark, dapat diakses dari lapomark.com, salah satu prioritas
            utama kami adalah privasi pengunjung kami. Dokumen Kebijakan Privasi
            ini berisi jenis informasi yang dikumpulkan dan dicatat oleh
            LapoMark dan bagaimana kami menggunakannya.
          </p>

          <h1 class="heading-tertiary">
            <span class="heading-tertiary">File Log</span>
          </h1>

          <p class="paragraph">
            LapoMark mengikuti prosedur standar menggunakan file log. File-file
            ini mencatat pengunjung ketika mereka mengunjungi situs web. Semua
            perusahaan hosting melakukan ini dan bagian dari analisis layanan
            hosting. Informasi yang dikumpulkan oleh file log termasuk alamat
            protokol internet (IP), jenis browser, Penyedia Layanan Internet
            (ISP), cap tanggal dan waktu, halaman rujukan / keluar, dan mungkin
            jumlah klik. Ini tidak terkait dengan informasi apa pun yang dapat
            diidentifikasi secara pribadi. Tujuan dari informasi ini adalah
            untuk menganalisis tren, mengelola situs, melacak pergerakan
            pengguna di situs web, dan mengumpulkan informasi demografis.
          </p>

          <h1 class="heading-tertiary">
            <span class="heading-tertiary">Privacy Policies</span>
          </h1>

          <p class="paragraph">
            Anda dapat berkonsultasi daftar ini untuk menemukan Kebijakan
            Privasi untuk masing-masing mitra iklan LapoMark. Kebijakan Privasi
            kami dibuat dengan bantuan Generator Kebijakan Privasi.
          </p>

          <p class="paragraph">
            Server iklan pihak ketiga atau jaringan iklan menggunakan teknologi
            seperti cookie, JavaScript, atau Web Beacon yang digunakan dalam
            iklan masing-masing dan tautan yang muncul di LapoMark, yang dikirim
            langsung ke peramban pengguna. Mereka secara otomatis menerima
            alamat IP Anda ketika ini terjadi. Teknologi ini digunakan untuk
            mengukur efektivitas kampanye iklan mereka dan / atau untuk
            mempersonalisasi konten iklan yang Anda lihat di situs web yang Anda
            kunjungi.
          </p>
        </div>
      </section>
    </main>
@endsection
