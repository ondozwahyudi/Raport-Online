
@extends('layouts.main_navigation')
@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('assets/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection
@section('contents_page')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Data Table</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Data Siswa</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      @if (session("success"))
      <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;
              </span>
          </button>

          {{ session("success")}}
          <strong> Well done!
          </strong>
      </div>
      @endif

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
            <h3 class="card-title">Data Table With Full Features</h3>
              <div style="
                float:right;
                position: absolute;
                top: 15px;
                right: 10px;">
                  <!-- Button trigger modal -->
                  <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Tambah Data
                </div>
            </div>
            <!-- /.card-header -->
          <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">
                  <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                            <tr>
                              <th>Ranking</th>
                              <th>Nis</th>
                              <th>Nama Siswa</th>
                              <th>Kelas</th>
                              <th>Tanggal Lahir</th>
                              <th>Jenis Kelamin</th>
                              <th>Agama</th>
                              <th>Alamat</th>
                              <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                              @php
                              $rangking = 1;    
                              @endphp
                            @foreach ($data_siswa as $data)
                            <tr>
                              <td>  {{$rangking}}</td>
                              <td>{{ $data->nis }}</td>
                              <td><a href="{{route('page_profil', $data->id)}}"> {{ $data->nama_siswa }}</a></td>
                              <td> {{ $data->kelas['kelas_nama']}} </td>
                              <td> {{ $data->tanggal_lahir }} </td>
                              <td> {{ $data->jenis_kelamin }}</td>
                              <td> {{ $data->agama }} </td>
                              <td> {{ $data->alamat}} </td>
                              <td>
                                <a href="{{ route('edit_siswa', $data->id)}}" class="btn btn-sm btn-info"><i class="fa fa-pencil-square"></i></a>
                                <a href="{{ route('delete_siswa', $data->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>
                              </td>
                            </tr>
                            @php
                            $rangking++;    
                            @endphp
                            @endforeach
                            </tbody> 
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Siswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{-- edit for name  --}}
      <div class="modal-body">
          <form action="{{route('add_action')}}" method="POST">
              <div class="box-body">
                <div class="form-group {{ $errors->has('nis') ? 'has-error' : '' }}">
                  <label for="nis">Nis</label>
                  <input type="text" class="form-control" id="nis" name="nis" placeholder="Nis" 
                   @if ($data_siswa->isEmpty())
                     value="{{ isset($i) ? $i++ : $i = 190001}}"
                   @else
                     value = "{{(max([$data->nis])+1)}}"
                  @endif
                  autocomplete="off" required>
                  @if ($errors->has('nis'))
                    <span class="help-block">{{$errors->first('nis')}}</span>
                  @endif  
                </div>

                <div class="form-group {{ $errors->has('kelas_id') ? 'has-error' : ''}} ">
                  <label>Kelas</label>
                  <select name="kelas_id" class="form-control" required>
                    <option value="">Pilih Kelas</option>
                    @foreach ($data_kelas as $kelas)
                    <option value="{{$kelas->id}}"> {{$kelas->kelas_nama}} </option>
                    @endforeach
                  </select>
                  @if ($errors->has('kelas_id'))
                    <span class="help-block">{{$errors->first('kelas_id')}}</span>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('nama_siswa') ? 'has-error' : ''}}">
                  <label for="nama_siswa">Nama Siswa</label>
                <input type="text" class="form-control" id="nama_siswa" value="{{old('nama_siswa')}}" name="nama_siswa" placeholder="Nama Siswa" autocomplete="off" required>
                  @if ($errors->has('nama_siswa'))
                    <span class="help-block">{{$errors->first('nama_siswa')}}</span>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('jenis_kelamin') ? 'has-error' : ''}}" >
                  <label for="jenis_kelamin">Jenis Kelamin</label>
                  <select name="jenis_kelamin" id="jenis_kelamin"  class="form-control" required>
                      <option value="">Jenis Kelamin</option>
                      <option value="L" {{(old('jenis_kelamin') == 'L') ? 'selected' : '' }} >Laki-Laki</option>
                      <option value="P" {{(old('jenis_kelamin') == 'P') ? 'selected' : '' }} >Prempuan</option>
                  </select>
                  @if ($errors->has('jenis_kelamin'))
                    <span class="help-block">{{$errors->first('jenis_kelamin')}}</span>
                  @endif
                </div>

                <div class="form-group {{$errors->has('tanggal_lahir') }}">
                  <label for="tanggal_lahir">Tanggal Lahir</label>
                  <input type="date" class="form-control" id="tanggal_lahir" value="{{old('tanggal_lahir')}}" name="tanggal_lahir" placeholder="Tanggal Lahir" required>
                  @if ($errors->has('tanggal_lahir'))
                    <span class="help-block">{{$errors->first('tanggal_lahir')}}</span>
                  @endif                
                </div>

                <div class="form-group {{ $errors->has('agama') ? 'has-error' : ''}}">
                  <label for="agama">Agama</label>
                  <select name="agama" id="agama" name="agama" class="form-control">
                      <option value="">Pilih Agama Anda</option>
                      <option value="Islam"{{(old('agama') == 'Islam') ? 'selected' : '' }}>Islam</option>
                      <option value="Kristen" {{(old('agama') == 'Kriste') ? 'selected' : '' }}>Kristen</option>
                      <option value="Hindu" {{(old('agama') == 'Hindu') ? 'selected' : '' }}>Hindu</option>
                      <option value="Budha" {{(old('agama') == 'Budha') ? 'selected' : '' }}>Budha</option>
                      <option value="Lainnya" {{(old('agama') == 'Lainnya') ? 'selected' : '' }}>Lainnya</option>
                  </select>
                  @if ($errors->has('agama'))
                    <span class="help-block">{{$errors->first('agama')}}</span>
                  @endif  
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{old('email')}}" autocomplete="off" required>
                  @if ($errors->has('email'))
                    <span class="help-block">{{$errors->first('email')}}</span>
                  @endif 
                </div>

                <div class="form-group {{ $errors->has('nama_ortu') ? 'has-error' : ''}}">
                  <label for="nama_ortu">Nama Orang Tua</label>
                  <input type="text" class="form-control" id="nama_ortu" name="nama_ortu" placeholder="Nama Orang Tua" value="{{old('nama_ortu')}}" autocomplete="off" required>
                  @if ($errors->has('nama_ortu'))
                   <span class="help-block">{{$errors->first('nama_ortu')}}</span>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('pekerjaan') ? 'has-error' : ''}}">
                  <label for="pekerjaan_orang_tua">Pekerjaan Orang Tua</label>
                  <input type="text" class="form-control" id="pekerjaan_orang_tua" name="pekerjaan" placeholder="Pekerjaan Orang Tua" value="{{old('pekerjaan')}}" autocomplete="off" required>
                  @if ($errors->has('pekerjaan'))
                    <span class="help-block">{{$errors->first('pekerjaan')}}</span>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('alamat') ? 'has-error' : ''}}">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" id="alamat" name="alamat" placeholder="Alamat" autocomplete="off" required>{{old('alamat')}}
                  </textarea>
                  @if ($errors->has('alamat'))
                    <span class="help-block">{{$errors->first('alamat')}}</span>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('no_telp') ? 'has-error' : ''}}">
                  <label for="nomer_telp">Nomer Telp</label>
                  <input type="text" class="form-control" id="nomer_telp" name="no_telp" placeholder="Nomer Telp" value="{{old('no_telp')}}" autocomplete="off" required>
                  @if ($errors->has('no_telp'))
                    <span class="help-block">{{$errors->first('no_telp')}}</span>
                  @endif
                  </div>

              <!-- /.box-body -->
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <input type="hidden" value="{{ Session::token() }}" name="_token">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer')
<!-- DataTables -->
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.bootstrap4.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>

@endsection