
@extends('layouts.main_navigation')
@section('header')

@endsection
@section('contents_page')
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
          <div class="row mb-2">
          <div class="col-sm-6">
              <h1>Data Table</h1>
          </div>
          <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item active"><a href="#">Data Mapel</a></li>
              <li class="breadcrumb-item active">Edit Data Mapel</li>
              </ol>
          </div>
          </div>
      </div>
  </section>
        @if (session("success"))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;
                </span>
            </button>
  
            {{ session("success")}}
            <strong> Well done!
            </strong>
        </div>
        @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="box">
            <div class="modal-body">
                <form action="{{route('get_update_mapel', $mapel->id)}}" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('kode') ? 'has-error' : '' }}">
                            <label for="kode">Kode Mata Pelajaran</label>
                            {{-- auto kode --}}
                            <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Mata Pelajaran" value = "{{ $mapel->kode}}" autocomplete="off" required>
                            @if ($errors->has('kode'))
                                <span class="help-block">{{$errors->first('kode')}}</span>
                            @endif  
                        </div>
                        <div class="form-group {{ $errors->has('mapel') ? 'has-error' : ''}}">
                        <label for="mapel">Mata Pelajaran</label>
                            <input type="text" class="form-control" id="mapel" name="mapel" value="{{$mapel->mapel}} " placeholder="Mata Pelajaran" autocomplete="off" required>
                            @if ($errors->has('mapel'))
                                <span class="help-block">{{$errors->first('mapel')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('guru_id') ? 'has-error' : ''}}" >
                            <label for="guru">Guru</label>
                            <select name="guru_id" id="guru"  class="form-control" required>
                                <option value=" {{$mapel->guru['id']}}"> {{$mapel->guru['nama_guru'] }} </option>
                                @foreach ($data_guru as $guru)
                                <option value="{{$guru->id}}">{{$guru->nama_guru}} </option>
                                @endforeach
                            </select>
                            @if ($errors->has('guru_id'))
                                <span class="help-block">{{$errors->first('guru_id')}}</span>
                            @endif
                        </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                </form>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>
@endsection
@section('footer')


@endsection