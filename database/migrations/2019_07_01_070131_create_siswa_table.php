<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('kelas_id');
            $table->integer('nis');
            $table->string('nama_siswa');
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('nama_ortu');
            $table->string('agama');
            $table->string('alamat');
            $table->string('pekerjaan');
            $table->string('no_telp');
            $table->string('upload_foto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
