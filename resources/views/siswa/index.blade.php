@extends('layouts.main_navigation', ['module' => 'page_dashboard'])
@section('header')
<style>
    table {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      border: 1px solid #ddd;
    }
    
    th, td {
      text-align: left;
      padding: 8px;
    }
    
    tr:nth-child(even){background-color: #f2f2f2}
  </style>
@endsection
@section('contents_page')
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>
    
        <section class="content">
          <div class="row">
            <div class="col-md-3 col-xs-6">      
                    <!-- About Me Box -->
              <div class="card card-primary">
                <div class="card-header with-border">
                  <h3 class="card-title">Info Kelas</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body">
                  <strong><i class="fa fa-book margin-r-5"></i>Nama Wali Kelas</strong>
                    <p class="text-muted">
                      {{$siswa->kelas->guru->nama_guru}}
                    </p>
                  <strong><i class="fa fa-map-marker margin-r-5"></i>Alamat Wali Kelas</strong>
                    <p class="text-muted">
                        {{$siswa->kelas->guru->alamat}}
                    </p>
          
                  <strong><i class="fa fa-phone margin-r-5"></i>Contact</strong>
                    <p class="text-muted">
                        {{$siswa->kelas->guru->no_telp}}
                    </p>
                </div>
              <!-- /.box -->
              </div>
          </div>
          <div class="col-md-9 col-6" >
            <div style="overflow-x:auto;">
                <div class="card card-primary card-outline"> 
                  <table >
                    <tr style="background:#007bff!important;">
                      <th style="color:#fff">Kode</th>
                      <th style="color:#fff">Mata Pelajaran</th>
                      <th style="color:#fff">Nama Guru</th>
                      <th style="color:#fff">Nilai</th>
                    </tr>
                    @foreach ($siswa->mapel as $mapel)      
                    <tr>
                      <td>{{$mapel->kode}}</td>
                      <td>{{$mapel->mapel}}</td>
                      @if ($mapel->guru["nama_guru"] == null)   
                        <td style="width:300px">kosong</td>
                      @else 
                        <td style="width:300px"> {{$mapel->guru["nama_guru"]}} </td>
                      @endif
                      <td>{{$mapel->pivot->nilai}}</td>
                    </tr>
                    @endforeach
                    <tr>
                      <td><b>Jumlah Rata-Rata</b></td>
                      <td></td>
                      <td></td>
                      <td><b>{{ $siswa->rata_rata() }}</b></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>   
@endsection