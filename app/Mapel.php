<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $table = "mapel";
    protected $fillable = ['kode','mapel','guru_id','semester_id'];


    //relasi table siswa
    public function siswa()
    {
        return $this->belongsToMany(Siswa::class)->withPivot(['nilai']);
    }

    public function guru()
    {
        return $this->belongsTo(Guru::class);
    }
    
    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }
}
