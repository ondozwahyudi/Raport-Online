@servers(['web' => 'root@45.77.41.110'])

@setup
    $repository = 'git@gitlab.com:ondozwahyudi/Raport-Online.git';
    $releases_dir = '/var/www/raport';
    $staging_dir = $releases_dir.'/raport/app';
    $app_dir_staging = '/var/www/raport';
    $env_dir = '/var/www/environment';
    $release = date('dmYHis');
    $staging_release_dir = $staging_dir.'/'.$release;
@endsetup
@story('deploy')
    clone_repository
    run_composer
    update_symlinks
    migrate
@endstory

@task('clone_repository', ['on' => 'web'])
    echo 'Delete all release (Staging)'
    rm -rf {{ $staging_dir }}/*
    echo 'Done'

    echo 'Cloning repository (Staging)'
    [ -d {{ $staging_dir }} ] || mkdir {{ $staging_dir }}
    git clone -b {{ $branch }} {{ $repository }} {{ $staging_release_dir }}
    echo 'Done'
@endtask

@task('run_composer', ['on' => 'web'])
    echo "Starting deployment ({{ $release }}) (Staging)"
    cd {{ $staging_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
    echo 'Done'
@endtask

@task('update_symlinks', ['on' => 'web'])
    echo "Linking storage directory (Staging)"
    rm -rf {{ $staging_release_dir }}/storage
    ln -nfs {{ $env_dir }}/poskop/storage {{ $staging_release_dir }}/storage
    echo 'Done'

    echo 'Linking .env file (Staging)'
    ln -nfs {{ $env_dir }}/.env.poskop {{ $staging_release_dir }}/.env
    echo 'Done'

    echo 'Linking current release (Staging)'
    ln -nfs {{ $staging_release_dir }} {{ $app_dir_staging }}/app
    echo 'Done'
@endtask

@task('migrate', ['on' => 'web'])
    echo 'Running Migrations (Staging)'
    cd {{ $app_dir_staging }}/app
    php artisan config:cache
    php artisan config:clear
    php artisan migrate:fresh
    php artisan db:seed
@endtask

