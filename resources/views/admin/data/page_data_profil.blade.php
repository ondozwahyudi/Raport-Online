@extends('layouts.main_navigation')
@section('header')
{{-- // stylis links css --}}
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<style>
.glyphicon-ok::before {
    content: "Ok";
}
.glyphicon-remove::before {
    content: "Cancel";
}
.glyphicon {
    font-family: 'arial';
    font-weight: 500;
    font-style: normal;
}

</style>
@endsection
@section('contents_page')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Table</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <div style="float:right;margin:5px; ">
                <!-- Button trigger modal -->
              <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#exampleModal">
              Tambah Nilai
            </div>
          </ol>
        </div>
      </div>
    </div>
  </section>
  @if (session("success"))
  <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;
          </span>
      </button>
      {{ session("success")}}
      <strong> Well done!
      </strong>
  </div>
  @endif
  @if (session("error"))
  <div class="alert alert-error" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;
          </span>
      </button>
      {{ session("error")}}
  </div>
  @endif    
  
  <section class="content">
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{$siswa->getPhoto()}}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$siswa->nama_siswa}}</h3>
                <p class="text-muted text-center">Software Engineer</p>
                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Kelas</b> <a class="float-right">{{$siswa->kelas["kelas_nama"]}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Rata-Rata</b> <a class="float-right">{{$siswa->rata_rata()}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Wali Kelas</b> <a class="float-right">{{$siswa->kelas->guru["nama_guru"]}}</a>
                  </li>
                </ul>
              </div>
          </div>
                <!-- About Me Box -->
          <div class="card card-primary card-outline"> 
            <div class="card-body">           
              <div class="box-header with-border">
                <h3 class="box-title">Bio Data</h3>
              </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class=""></i>Tanggal Lahir</strong>
              <p class="text-muted">
                  {{ $siswa->tanggal_lahir}}
              </p>
              <strong><i class=""></i>Nama Orang Tua</strong>
              <p class="text-muted">
                  {{ $siswa->nama_ortu}}
              </p>    
              <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>
                  <p class="text-muted">{{$siswa->alamat }}</p>      
              <hr>    
              <strong><i class="fa fa-map-marker margin-r-5"></i>Contact</strong>
                <p class="text-muted">
                  Phone : {{$siswa->no_telp}}
                </p>
                <p class="text-muted">
                  Email : {{$siswa->user()->email}}
                </p>
              <hr>        
              </div>
            </div>
          </div>
        </div>        
        <div class="col-9 col-6" >
          <div style="overflow-x:auto;">
              <div class="card card-primary card-outline"> 
              <table id="example1" class="table table-striped">
                <tr style="background:#3c8dbc;">
                  <th>Kode</th>
                  <th>Mata Pelajaran</th>
                  <th>Nama Guru</th>
                  <th>Nilai</th>
                  <th>Aksi</th>
                </tr>
                  @foreach ($siswa->mapel as $mapel)
                  <tr>                        
                    <td style="width:100px">{{$mapel->kode}}</td>
                    <td style="width:200px">{{$mapel->mapel}}</td>
                    @if ($mapel->guru["nama_guru"] == null)   
                      <td style="width:300px">kosong</td>
                    @else 
                    <td style="width:300px"> {{$mapel->guru["nama_guru"]}} </td>
                    @endif
                    <td>
                      <a href="#" class="mapeledit" data-type="text" data-pk="{{$mapel->id}}" data-url="{{route('editnilai', $siswa->id)}}" data-title="Masuk nilai">{{$mapel->pivot->nilai}}</a>
                    </td>
                    <td style="width:100px">
                      <a href="{{route('deletenilai',[$siswa->id ,$mapel->id])}}" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
          </div>
            <div class="card card-primary">
              <div class="card-body">
                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title">Grafic Nilai</h3>
                  </div>
                  <div class="panel-body">
                    <div id="chartNilai" name="chartNilai">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Siswa</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {{-- edit for name  --}}
        <div class="modal-body">
          <form action="{{route('add_nilai', $siswa->id)}}" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group {{ $errors->has('mapel') ? 'has-error' : ''}}" >
                    <label for="mapel">Mata Pelajaran</label>
                    <select name="mapel" id="mapel"  class="form-control" required>
                      <option value="">Pilih Mata Pelajaran</option>
                      @foreach ($mapell as $mp)                      
                      <option value="{{ $mp->id}}">{{$mp->mapel}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('mapel'))
                      <span class="help-block">{{$errors->first('mapel')}}</span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('nilai') ? 'has-error' : ''}}">
                    <label for="nilai">Nilai</label>
                      <input type="text" class="form-control" id="nilai" value="{{old('nilai')}}" name="nilai" placeholder="Nilai" autocomplete="off" required>
                    @if ($errors->has('nilai'))
                      <span class="help-block">{{$errors->first('nilai')}}</span>
                    @endif
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              <input type="hidden" value="{{ Session::token() }}" name="_token">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection


@section('footer')

<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/oldie.js"></script>
<script>
  // //turn to inline mode popup
  // $.fn.editable.defaults.mode = 'inline';
  // //edit tables
  // $(document).ready(function() {
  //       $('.mapeledit').editable();
  //   });

    $(document).ready(function() {
        $('.mapeledit').editable({
            mode: 'inline',
        });
        $('.mapeledit').editable({
            mode: 'inline',
            type: 'number',
            step: '1.00',
            min: '0.00',
            max: '24'
        });
    });
  //chart
  Highcharts.chart('chartNilai', {
    chart: {
        type: 'column',
    },
    title: {
        text: 'Laporan Nilai Siswa'
    },
    xAxis: {
        categories: {!!json_encode($categories)!!} ,
        // echo spesial untuk menampilkan seperti di json !!
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Nilai'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        //     '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 5
        }
    },
    series: [{
        name: 'Nilai',
        // colorByPoint: true,
        data:{!!json_encode($data_nilai)!!}
    }],
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});

</script>
    
@endsection