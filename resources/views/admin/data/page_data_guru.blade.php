
@extends('layouts.main_navigation')
@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('assets/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection
@section('contents_page')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Data Table</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Data Guru</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      @if (session("success"))
      <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;
              </span>
          </button>

          {{ session("success")}}
          <strong> Well done!
          </strong>
      </div>
      @endif

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="box-title">Data Table With Full Features</h3>
              <div style="
              float:right;
              position: absolute;
              top: 15px;
              right: 10px; ">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
                  Tambah data
              </div>
            </div>
            <!-- /.box-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama Guru</th>
                  <th>Alamat</th>
                  <th>Nomer telp</th>
                  <th>Email</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($data_guru as $guru)       
                  <tr>
                    <td>{{$guru->nama_guru}}</td>
                    <td>{{$guru->alamat}}</td>
                    <td>{{$guru->no_telp }}</td>
                    <td>{{$guru->user()->email}}</td>
                    <td>
                        <a href="{{ route('edit_guru', $guru->id)}}" class="btn btn-sm btn-info"><i class="fa fa-pencil-square"></i></a>
                        <a href="{{ route('delete_guru', $guru->id)}}" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Siswa</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {{-- edit for name  --}}
        <div class="modal-body">
            <form action="{{route('add_action_guru')}}" method="POST">
                <div class="box-body">
  
                  <div class="form-group {{ $errors->has('nama_guru') ? 'has-error' : ''}}">
                    <label for="nama_guru">Nama Guru</label>
                  <input type="text" class="form-control" id="nama_guru" value="{{old('nama_guru')}}" name="nama_guru" placeholder="Nama Guru" autocomplete="off" required>
                    @if ($errors->has('nama_guru'))
                      <span class="help-block">{{$errors->first('nama_guru')}}</span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                      <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" value="{{old('email')}}" name="email" placeholder="Email" autocomplete="off" required>
                      @if ($errors->has('email'))
                        <span class="help-block">{{$errors->first('email')}}</span>
                      @endif
                    </div>
    
                  <div class="form-group {{ $errors->has('alamat') ? 'has-error' : ''}}">
                    <label for="alamat">Alamat</label>
                    <textarea class="form-control" id="alamat" name="alamat" placeholder="Alamat" autocomplete="off" required>{{old('alamat')}}
                    </textarea>
                    @if ($errors->has('alamat'))
                      <span class="help-block">{{$errors->first('alamat')}}</span>
                    @endif
                  </div>
  
                  <div class="form-group {{ $errors->has('no_telp') ? 'has-error' : ''}}">
                    <label for="nomer_telp">Nomer Telp</label>
                    <input type="text" class="form-control" id="nomer_telp" name="no_telp" placeholder="Nomer Telp" value="{{old('no_telp')}}" autocomplete="off" required>
                    @if ($errors->has('no_telp'))
                      <span class="help-block">{{$errors->first('no_telp')}}</span>
                    @endif
                    </div>
  
                <!-- /.box-body -->
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              <input type="hidden" value="{{ Session::token() }}" name="_token">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('footer')
<!-- DataTables -->
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.bootstrap4.js')}}"></script>
<!-- page script -->
<script>
        $(function () {
          $('#example1').DataTable()
          $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
          })
        })
      </script>
@endsection



