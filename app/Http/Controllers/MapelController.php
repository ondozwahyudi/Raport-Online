<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\User;
use App\Guru;
use App\Mapel;

class MapelController extends Controller
{
    public function index()
    {
        $mapel = Mapel::all();
        $data_guru = Guru::all();
        // return response()->json($mapel);
        return view('admin.mapel.page_mapel', ['mapel'=> $mapel, 'data_guru' => $data_guru ]);
    }
    //add data Matapelajaran
    public function addmapel(Request $request)
    {
        $mapel = Mapel::create($request->all());
        return redirect()->route('admin_page_mapel')->with('success', 'You seccessfully add');
    }
    //delate
    public function getdelete($id)
    {
        $mapel = Mapel::find($id);
        $mapel->delete();
        return redirect()->back()->with('success', 'You successfully add');
    }
    //get edit
    public function getedit($id)
    {
        $mapel = Mapel::find($id);
        $data_guru = Guru::all();
        return view('admin.mapel.page_mapel_edit',['mapel'=>$mapel, 'data_guru' => $data_guru]);
    }
    //get update  mapel
    public function getupdate(Request $request,$id){
        $mapel = Mapel::find($id);
        $mapel->update($request->all());
        return redirect()->route('admin_page_mapel')->with('success', ' You successfully Update');
    }

}