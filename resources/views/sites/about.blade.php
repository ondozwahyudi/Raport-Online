@extends('layouts.frontend')
@section('contents')

    <header class="header">
      <div class="header__logo-box">
        <img src="{{asset('/frontend')}}/img/lapomark.png" alt="Logo" class="header__logo" />
      </div>

      <div class="header__text-box">
        <h1 class="heading-primary">
          <span class="heading-primary--large">Frequently Asked Questions</span>
        </h1>
        <p class="paragraph paragraph--white">
          Berikut adalah pertanyaan yang umum ditanyakan kepada kami.
        </p>

        <a href="#" class="btn btn--white btn--animated">Lihat Pertanyaan</a>
      </div>
    </header>

    <main>
      <section class="section-price">
        <div class="u-center-text ">
          <h2 class="heading-secondary u-margin-bottom-medium">
            Real-time Monitoring Nilai Murid.
          </h2>
        </div>

        <div class="row">
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--gilang">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--1">
                    Gilang Wahyudi
                  </span>
                </h4>

                <div class="card__details">
                  <div class="card__details--team">
                    <h4>( Backend Developer )</h4>

                    <p>
                      Backend Developer yang sedang menekuni Laravel. Rela tidak
                      tidur berhari-hari demi memperbaiki error. Sama error aja
                      setia, apalagi kamu, Hiyaa.
                    </p>
                  </div>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-1">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">
                      Instagram <br />@gilang_wahyudi09
                    </p>
                    <p class="card__price-value">
                      <i class="fab fa-instagram"></i>
                    </p>
                  </div>

                  <a
                    href="https://www.instagram.com/gilang_wahyudi09/"
                    class="btn btn--white"
                    >Follow</a
                  >
                </div>
              </div>
            </div>
          </div>
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--apenk">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--2">
                    Afochar SyabniR
                  </span>
                </h4>

                <div class="card__details">
                  <div class="card__details--team">
                    <h4>( Front-End Developer )</h4>
                    <p>
                      Front-End Developer yang kerjanya ngopi ngudud dan
                      bersantai. Sekarang sedang mempelajari HTML, CSS, dan
                      Javascript dan PUBG.
                    </p>
                  </div>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-2">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Instagram <br />@apejakno</p>
                    <p class="card__price-value">
                      <i class="fab fa-instagram"></i>
                    </p>
                  </div>

                  <a
                    href="https://www.instagram.com/apejakno/"
                    class="btn btn--white"
                    >Follow</a
                  >
                </div>
              </div>
            </div>
          </div>
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--dimas">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--3">
                    Azaqi Dhimas Prayoga
                  </span>
                </h4>

                <div class="card__details">
                  <div class="card__details--team">
                    <h4>( Animator )</h4>
                    <p>
                      Seorang Animator yang menyukai ke-Sampoerna-an. Passion
                      dan Hobbinya adalah rebahan, kemudian tidur. Ia
                      sangat-sangat mencintai kasurnya.
                    </p>
                  </div>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-3">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Instagram <br />@dhimasazaki</p>
                    <p class="card__price-value">
                      <i class="fab fa-instagram"></i>
                    </p>
                  </div>

                  <a
                    href="https://www.instagram.com/dhimasazaki/"
                    class="btn btn--white"
                    >Follow</a
                  >
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--sidik">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--3">
                    Sidik Ristiawan Yusuf
                  </span>
                </h4>

                <div class="card__details">
                  <div class="card__details--team">
                    <h4>( Database Administrator )</h4>
                    <p>
                      Database Administrator yang sedang mempelajari query-query
                      cinta. Untuk mengkoneksikan database cinta yang bersumber
                      dari kedalaman jiwa.
                    </p>
                  </div>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-3">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Instagram <br />@ssidik.28</p>
                    <p class="card__price-value">
                      <i class="fab fa-instagram"></i>
                    </p>
                  </div>

                  <a
                    href="https://www.instagram.com/ssidik.28/"
                    class="btn btn--white"
                    >Follow</a
                  >
                </div>
              </div>
            </div>
          </div>

          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--yakob">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--2">
                    Yakob Nurhuda
                  </span>
                </h4>

                <div class="card__details">
                  <div class="card__details--team">
                    <h4>( Content Creator )</h4>
                    <p>
                      Content creator yang sangat jago memainkan kata, terutama
                      jika berada di depan wanita. Jika wanita sudah teracuni,
                      maka ia hanya bisa pasrah saja.
                    </p>
                  </div>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-2">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">
                      Instagram <br />
                      @yakobnh_
                    </p>
                    <p class="card__price-value">
                      <i class="fab fa-instagram"></i>
                    </p>
                  </div>

                  <a
                    href="https://www.instagram.com/yakobnh_/"
                    class="btn btn--white"
                    >Follow</a
                  >
                </div>
              </div>
            </div>
          </div>

          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--nuria">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--1">
                    Nuria
                  </span>
                </h4>

                <div class="card__details">
                  <div class="card__details--team">
                    <h4>( UI/UX Designer )</h4>
                    <p>
                      Seorang Fotografer Selfier Instagramer yang sangat hobi
                      cekrak cekrek, lalu kemudian upload di Instagram. Entah,
                      itu sepertinya pekerjaan semua wanita. Mungkinn...
                    </p>
                  </div>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-1">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">
                      Instagram <br />@nurya_arafah13
                    </p>
                    <p class="card__price-value">
                      <i class="fab fa-instagram"></i>
                    </p>
                  </div>

                  <a
                    href="https://www.instagram.com/nurya_arafah13/"
                    class="btn btn--white"
                    >Follow</a
                  >
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
@endsection
