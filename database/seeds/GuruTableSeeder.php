<?php

use Illuminate\Database\Seeder;

class GuruTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('guru')->delete();
        
        \DB::table('guru')->insert(array (
            0 => 
            array (
                'id' => 5,
                'user_id' => 42,
                'nama_guru' => 'Herman Agustin',
                'alamat' => 'Yogyakarta',
                'no_telp' => '081918135527',
                'created_at' => '2019-07-03 11:18:02',
                'updated_at' => '2019-07-03 11:18:02',
            ),
            1 => 
            array (
                'id' => 6,
                'user_id' => 43,
                'nama_guru' => 'Suharno',
                'alamat' => 'Yogyakarta',
                'no_telp' => '082211182112',
                'created_at' => '2019-07-03 11:41:49',
                'updated_at' => '2019-07-03 11:41:49',
            ),
            2 => 
            array (
                'id' => 7,
                'user_id' => 44,
                'nama_guru' => 'Kandoyo',
                'alamat' => 'Yogyakarta',
                'no_telp' => '081912771811',
                'created_at' => '2019-07-03 11:42:22',
                'updated_at' => '2019-07-03 11:42:22',
            ),
            3 => 
            array (
                'id' => 8,
                'user_id' => 45,
                'nama_guru' => 'Firmasah',
                'alamat' => 'Bantul',
                'no_telp' => '082333112456',
                'created_at' => '2019-07-03 11:43:04',
                'updated_at' => '2019-07-03 11:43:04',
            ),
            4 => 
            array (
                'id' => 9,
                'user_id' => 46,
                'nama_guru' => 'Rusmanto',
                'alamat' => 'Lombok',
                'no_telp' => '087331836411',
                'created_at' => '2019-07-03 11:43:46',
                'updated_at' => '2019-07-03 11:43:46',
            ),
            5 => 
            array (
                'id' => 10,
                'user_id' => 47,
                'nama_guru' => 'Sri Martini',
                'alamat' => 'Sleman',
                'no_telp' => '085664354675',
                'created_at' => '2019-07-03 11:44:40',
                'updated_at' => '2019-07-03 11:44:40',
            ),
            6 => 
            array (
                'id' => 11,
                'user_id' => 48,
                'nama_guru' => 'Sri Ningsih',
                'alamat' => 'Sleman',
                'no_telp' => '087223531265',
                'created_at' => '2019-07-03 11:45:41',
                'updated_at' => '2019-07-03 11:45:41',
            ),
            7 => 
            array (
                'id' => 12,
                'user_id' => 49,
                'nama_guru' => 'Subono',
                'alamat' => 'Gunung Kidul',
                'no_telp' => '0876567432887',
                'created_at' => '2019-07-03 11:46:20',
                'updated_at' => '2019-07-03 11:46:20',
            ),
            8 => 
            array (
                'id' => 13,
                'user_id' => 50,
                'nama_guru' => 'Maksum',
                'alamat' => 'Bantul',
                'no_telp' => '0823345678543',
                'created_at' => '2019-07-03 11:46:50',
                'updated_at' => '2019-07-03 11:46:50',
            ),
            9 => 
            array (
                'id' => 14,
                'user_id' => 51,
                'nama_guru' => 'Tugiar',
                'alamat' => 'Wonosari',
                'no_telp' => '081331456378',
                'created_at' => '2019-07-03 11:47:34',
                'updated_at' => '2019-07-03 11:47:34',
            ),
        ));
        
        
    }
}