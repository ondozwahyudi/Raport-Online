@extends('layouts.frontend')
@section('contents')

    <header class="header">
      <div class="header__logo-box">
        <img src="{{asset('/frontend')}}/img/lapomark.png" alt="Logo" class="header__logo" />
      </div>

      <div class="header__text-box">
        <h1 class="heading-primary">
          <span class="heading-primary--main">LapoMark</span>
          <span class="heading-primary--sub">Real-Time Monitoring Nilai</span>
          <span class="heading-primary--sub">followers</span>
          <span class="heading-primary--sub">{{$data->counts->followed_by}} </span>
        </h1>
        {{-- "followed_by": 351 --}}

        <a href="#about" class="btn btn--white btn--animated"
          >Lihat Selengkapnya</a
        >
      </div>
    </header>

    <main>
      <section class="section-about" id="about">
        <div class="u-center-text ">
          <h2 class="heading-secondary u-margin-bottom-large">
            Apa itu lapomark?
          </h2>
        </div>
        <div class="row">
          <div class="col-1-of-2">
            <h3 class="heading-tertiary u-margin-bottom-small">
              Website Raport online dengan berbagai fitur.
            </h3>
            <p class="paragraph">
              LapoMark adalah raport modern yang berbasis web dengan fitur utama
              real-time based monitoring . Diciptakan dengan tujuan memudahkan
              bagi Pendidikan kita. Sehingga Murid, Guru, dan Orang Tua dapat
              berkolaborasi dan menghasilkan output prestasi murid menjadi lebih
              baik.
            </p>

            <h3 class="heading-tertiary u-margin-bottom-small">
              Evaluasi hasil belajar anak anda
            </h3>
            <p class="paragraph">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit.
              Reiciendis nisi quasi iusto, facilis nobis labore, rem provident
            </p>

            <a href="#" class="btn-text">Baca Selengkapnya &rarr;</a>
          </div>
          <div class="col-1-of-2">
            <div class="composition">

            </div>
          </div>
        </div>
      </section>

      <section class="section-price">
        <div class="u-center-text ">
          <h2 class="heading-secondary u-margin-bottom-medium">
            Real-time Monitoring Nilai Murid.
          </h2>
        </div>

        <div class="row">
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--1">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--1">
                    Real Time Monitoring
                  </span>
                </h4>

                <div class="card__details">
                  <ul>
                    <li>30 Hari Aktif</li>
                    <li>Masa coba-coba</li>
                    <li>Beli dong</li>
                    <li>Biar tau rasanya</li>
                    <li>Enak kann</li>
                  </ul>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-1">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$297</p>
                  </div>

                  <a href="#" class="btn btn--white">Pesan Sekarang</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--2">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--2">
                    10 Top Rank Global
                  </span>
                </h4>

                <div class="card__details">
                  <ul>
                    <li>180 Hari Aktif</li>
                    <li>Kurang dong</li>
                    <li>Nambah Ah</li>
                    <li>Kali aja enak</li>
                    <li>Jadi keterusan</li>
                  </ul>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-2">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$697</p>
                  </div>

                  <a href="#" class="btn btn--white">Pesan Sekarang</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front">
                <div class="card__picture card__picture--3">
                  &nbsp;
                </div>

                <h4 class="card__heading">
                  <span class="card__heading-span card__heading-span--3">
                    Achieve the best prize
                  </span>
                </h4>

                <div class="card__details">
                  <ul>
                    <li>360 Hari Aktif</li>
                    <li>Cieee ketagihan</li>
                    <li>Kurang yaa</li>
                    <li>Sabar donggg</li>
                    <li>Tunggu waktunya</li>
                  </ul>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-3">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$897</p>
                  </div>

                  <a href="#" class="btn btn--white">Pesan Sekarang</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="u-center-text u-margin-top-large">
          <a href="#" class="btn btn--green">Lihat Semua Harga</a>
        </div>
      </section>

      <section class="section-stories">
        <div class="bg-video">
          <video class="bg-video__content" autoplay muted loop>
            <source src="{{asset('/frontend')}}/img/bg-video.mp4" type="video/mp4" />
          </video>
        </div>

        <div class="u-center-text u-margin-bottom-large">
          <h2 class="heading-secondary">
            Cerita dari pelanggan kami
          </h2>
        </div>

        <div class="row">
          <div class="story">
            <figure class="story__shape">
              <img src="{{asset('/frontend')}}/img/raisa.jpg" alt="Raisa" class="story__img" />
              <figcaption class="story__caption">Mantan Raisa</figcaption>
            </figure>

            <div class="story__text">
              <h3 class="heading-tertiary u-margin-bottom-small">
                Alhamdulillah saya bisa pukul anak saya
              </h3>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus
                similique atque rem ratione eaque molestiae voluptate quod amet
                facere quidem excepturi enim, non officia voluptatem, illum,
                consectetur fugiat eum nisi dicta possimus. Vel dolores quas
                eligendi exercitationem, odit, temporibus nobis distinctio eaque
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="story">
            <figure class="story__shape">
              <img src="{{asset('/frontend')}}/img/isyana.jpg" alt="Raisa" class="story__img" />
              <figcaption class="story__caption">Bebeb Isyana</figcaption>
            </figure>

            <div class="story__text">
              <h3 class="heading-tertiary u-margin-bottom-small">
                Alhamdulillah saya bisa pukul anak saya
              </h3>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus
                similique atque rem ratione eaque molestiae voluptate quod amet
                facere quidem excepturi enim, non officia voluptatem, illum,
                consectetur fugiat eum nisi dicta possimus. Vel dolores quas
                eligendi exercitationem, odit, temporibus nobis distinctio eaque
              </p>
            </div>
          </div>
        </div>

        <div class="u-center-text u-margin-bottom-large">
          <a href="#" class="btn-text">Baca Selengkapnya &rarr;</a>
        </div>
      </section>

      <section class="section-book">
        <div class="row">
          <div class="book">
            <div class="book__form">
              @if (session("success"))
              <div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;
                      </span>
                  </button>

                  {{ session("success")}}
                  <strong>
                    MOHON TUNGGU KONFIRMASI DARI KAMI 1X24
                  </strong>
              </div>
              @endif
              <form action="{{url("/send")}}" method="POST">
                {{ csrf_field() }}
                <div class="u-margin-bottom-medium">
                  <h2 class="heading-secondary">
                    Pesan sekarang
                  </h2>
                </div>
                <div class="form__group {{ $errors->has('nama') ? 'has-error' : '' }}">
                  <input
                    type="text"
                    class="form__input"
                    placeholder="Full Name"
                    id="name"
                    name="nama"
                    autocomplete="off"
                    required
                  />
                  <label for="name" class="form__label">Full Name</label>
                  @if ($errors->has('nama'))
                    <span class="help-block">{{$errors->first('nama')}}</span>
                  @endif
                </div>

                <div class="form__group {{ $errors->has('email') ? 'has-error' : '' }}">
                  <input
                    type="email"
                    class="form__input"
                    placeholder="E-mail"
                    name="email"
                    id="email"
                    autocomplete="off"
                    required
                  />
                  <label for="email" class="form__label">E-mail</label>
                   @if ($errors->has('email'))
                    <span class="help-block">{{$errors->first('email')}}</span>
                  @endif
                </div>

                <div class="form__group">
                  <div class="form__radio-group">
                    <input
                      type="radio"
                      class="form__radio-input"
                      id="one"
                      name="time"
                      value="1 Bulan"
                    />
                    <label for="one" class="form__radio-label">
                      <span class="form__radio-button" ></span> 1 Bulan
                    </label>
                  </div>

                  <div class="form__radio-group">
                    <input
                      type="radio"
                      class="form__radio-input"
                      id="six"
                      name="time"
                      value="6 Bulan"
                    />
                    <label for="six" class="form__radio-label">
                      <span class="form__radio-button"></span>6 Bulan
                    </label>
                  </div>

                  <div class="form__radio-group u-margin-bottom-medium">
                    <input
                      type="radio"
                      class="form__radio-input"
                      id="twelve"
                      name="time"
                      value="12 Bulan"
                    />
                    <label for="twelve" class="form__radio-label">
                      <span class="form__radio-button"></span>12 Bulan
                    </label>
                  </div>
                </div>

                <div class="form__group">
                  <button type="submit" class="btn btn--green">Selanjutnya &rarr;</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </main>
    <!--
    <section class="grid-test">
      <div class="row">
        <div class="col-1-of-2">
          Col 1 of 2
        </div>
        <div class="col-1-of-2">
          Col 1 of 2
        </div>
      </div>

      <div class="row">
        <div class="col-1-of-3">
          Col 1 of 3
        </div>
        <div class="col-1-of-3">
          Col 1 of 3
        </div>
        <div class="col-1-of-3">
          Col 1 of 3
        </div>
      </div>

      <div class="row">
        <div class="col-1-of-3">
          Col 1 of 3
        </div>
        <div class="col-2-of-3">
          Col 2 of 3
        </div>
      </div>

      <div class="row">
        <div class="col-1-of-4">
          Col 1 of 4
        </div>
        <div class="col-1-of-4">
          Col 1 of 4
        </div>
        <div class="col-1-of-4">
          Col 1 of 4
        </div>
        <div class="col-1-of-4">
          Col 1 of 4
        </div>
      </div>

      <div class="row">
        <div class="col-1-of-4">
          Col 1 of 4
        </div>
        <div class="col-1-of-4">
          Col 1 of 4
        </div>
        <div class="col-2-of-4">
          Col 2 of 4
        </div>
      </div>

      <div class="row">
        <div class="col-1-of-4">
          Col 1 of 4
        </div>
        <div class="col-3-of-4">
          Col 3 of 4
        </div>
      </div>
    </section>
    -->
@endsection
