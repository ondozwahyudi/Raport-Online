@extends('layouts.frontend')

@section('contents')
    <header>
        <div class="section-login">
          <div class="row">
            <div class="book">
              <div class="book__form">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                  <div class="u-margin-bottom-medium">
                    <h2 class="heading-secondary">
                      Masukkan Email
                    </h2>
                  </div>
  
                  <div class="form__group">
                    <input
                      type="email"
                      class="form__input @error('email') is-invalid @enderror"
                      placeholder="Masukkan E-mail"
                      name="email" 
                      value="{{ old('email') }}"
                      id="email"
                      autofocus
                      required
                    />
                    <label for="email" class="form__label">Masukkan E-mail</label>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>  
                  <div class="form__group">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn--green">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    </header>
@endsection
