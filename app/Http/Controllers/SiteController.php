<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Vinkla\Instagram\Instagram;
use Nexmo\Laravel\Facade\Nexmo;

class SiteController extends Controller
{
    public function index()
    {
        $instagram = new Instagram('2130148879.212a4a9.a88b3733c8464f59ac037b55190d67f4');
        $data  = $instagram->self();
        // return response()->json($data);
        $photo = $instagram->media(['count' => 5]);
        return view('sites.home',['photo' => $photo, 'data' => $data]); 
    }
    public function sendSms(Request $request)
    {
        //news
        $this->validate($request, [
            'email'     => 'required|email',
            'nama'      => 'required|min:3',

        ]);
        
        Nexmo::message()->send([
            'to'   => '6281918135527',
            'from' => 'NEXMO',
            'text' => ['Email : '.$request->email. ' Nama Pemesan : '.$request->nama. ' Paket :'.$request->time ],
        ]);
        // Session::flash('success', 'Berhasil Mengirim');
        return redirect()->route('home_site')->with('success', 'Berhasil terkirim');;
    }

    public function login()
    {
        return view('auth.login');
    }
    public function pesan()
    {
        return view('sites.book');
    }
    public function feature()
    {
        return view('sites.features');
    }
    public function price()
    {
        return view('sites.price');
    }
    public function story()
    {
        return view('sites.story');
    }
    public function about()
    {
        return view('sites.about');
    }
    public function faq()
    {
        return view('sites.faq');
    }
    public function privacy()
    {
        return view('sites.privacy');
    }
}
