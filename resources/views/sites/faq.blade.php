@extends('layouts.frontend')
@section('contents')
    <header class="header">
      <div class="header__logo-box">
        <img src="{{asset('/frontend')}}/img/lapomark.png" alt="Logo" class="header__logo" />
      </div>

      <div class="header__text-box">
        <h1 class="heading-primary">
          <span class="heading-primary--large">Frequently Asked Questions</span>
        </h1>
        <p class="paragraph paragraph--white">
          Berikut adalah pertanyaan yang umum ditanyakan kepada kami.
        </p>

        <a href="#" class="btn btn--white btn--animated">Lihat Pertanyaan</a>
      </div>
    </header>

    <main>
      <section class="section-faq">
        <div class="u-center-text ">
          <h2 class="heading-secondary u-margin-bottom-small">
            Pertanyaan Umum Yang Ditanyakan
          </h2>
        </div>

        <div class="row">
          <h1 class="heading-tertiary">
            <span class="heading-tertiary"
              >1. Apakah murid bisa melihat langsung nilainya?</span
            >
          </h1>

          <p class="paragraph">
            Tentu saja iya, dikarenakan kami sudah menggunakan sistem online
            yang berbasis real-time dimana nilai yang baru di input akan
            langsung dapat dilihat.
          </p>

          <h1 class="heading-tertiary">
            <span class="heading-tertiary"
              >2. Apakah kita tidak perlu lagi datang ke sekolah?</span
            >
          </h1>

          <p class="paragraph">
            Itu tergantung dari situasi dan kondisi anda. Jika anda sebagai
            orang tua disibukkan oleh aktivitas kerja, maka anda bisa secara
            langsung mencetak melalui website kami
          </p>

          <h1 class="heading-tertiary">
            <span class="heading-tertiary"
              >3. Apa kelebihan lainnya dari LapoMark?</span
            >
          </h1>

          <p class="paragraph">
            Kelebihan utamanya ialah dengan sistem yang sudah real-time maka
            anda bisa melihat 10 peringkat teratas dari murid di kelas.
          </p>

          <h1 class="heading-tertiary">
            <span class="heading-tertiary"
              >4. Apakah LapoMark merupakan website berbayar?</span
            >
          </h1>

          <p class="paragraph">
            Pada dasarnya, kami memiliki banyak team yang bekerjasama, sehingga
            diperlukan biaya untuk berbagai keperluan perusahaan terutama biaya
            rokok dan kopi.
          </p>
        </div>
      </section>
    </main>
@endsection